<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

$descuentoEfectivo= floatval($_POST['efectivo'])/100;
$descuentoBanco= $_POST['banco']/100;
$descuentoMercadoPago= $_POST['mercadoPago']/100;

// Consulta a la base de datos para obtener los cobros correspondientes al dia actual
$sql = "UPDATE descuentos 
        SET porcentaje = CASE
            WHEN id_descuento = 1 THEN $descuentoEfectivo
            WHEN id_descuento = 2 THEN $descuentoBanco
            WHEN id_descuento = 3 THEN $descuentoMercadoPago
        END";

// Enviar los datos de los gastos en formato JSON
if (mysqli_query($conn, $sql)) {
    echo json_encode(array("success" => "Proceso completado correctamente"));
} else {
    echo json_encode(array("error" => "Error en el proceso"));
}

$conn->close();

?>
