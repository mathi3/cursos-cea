<?php
// Configurar la conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar la conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Recibe el objeto JSON enviado desde JavaScript
$cursosYCuotasJSON = $_POST['cursosYCuotas'];

// Decodifica el JSON en un arreglo asociativo de PHP
$cursosYCuotas = json_decode($cursosYCuotasJSON, true);

// Inicia una transacción
mysqli_autocommit($conn, false);
$success = true;

// Verifico si ya hay una entrada en la caja diaria con la fecha de hoy, si no, la creo. Necesario para evitar errores con las claves foraneas
$fecha = date("Y-m-d"); // Obtén la fecha actual en el formato necesario
$sql_verificacion = "INSERT IGNORE INTO caja_diaria (fecha_caja, apertura) VALUES ('$fecha', '0')";
if (!mysqli_query($conn, $sql_verificacion)) {
    $success = false;
    echo "Error al verificar la caja diaria: " . mysqli_error($conn);
}

// Recorre el arreglo de cursosYCuotas e inserta los datos en la base de datos
foreach ($cursosYCuotas as $cursoYCuota) {
    $alumno = $cursoYCuota['alumno'];
    $curso = $cursoYCuota['curso'];
    $fecha = $cursoYCuota['fecha'];
    $monto = $cursoYCuota['subtotal'];
    $numCuota = $cursoYCuota['numeroCuota'];
    $id_descu = $cursoYCuota['id_descu'];
    $descuento = $cursoYCuota['descuento'];

    // Consulta SQL para obtener el valor de 'fecha_inicio' del curso en la tabla "cursos"
    $sql_obtener_fecha_inicio = "SELECT fecha_inicio FROM cursos WHERE id_curso = '$curso'";
    $result2 = mysqli_query($conn, $sql_obtener_fecha_inicio);
    $row2 = mysqli_fetch_assoc($result2);
    $fecha_inicio = new DateTime($row2['fecha_inicio']);

    //Calculo diferencia entre ambas fechas
    $fecha_actual = date("Y-m-d"); 
    $interval = $fecha_inicio->diff(new DateTime($fecha_actual)); //Calculo la diferencia entre ambas fechas
    $meses_de_diferencia = intval($interval->y * 12 + $interval->m); //Obtengo los meses de diferencia

    
        // Ejecutar consulta de actualización
        $sql_cuota = "UPDATE deudas SET cuotas_pagas = '$numCuota' WHERE id_alumno = '$alumno' AND id_curso = '$curso'";
        if (!mysqli_query($conn, $sql_cuota)) {
            $success = false;
            echo "Error al actualizar cuotas en deudas: " . mysqli_error($conn);
            break; // Sal del bucle si hay un error
        }
        
    // Consulta SQL para obtener el valor de 'cant_cuotas' de la tabla 'deudas'
    $sql_obtener_cant_cuotas = "SELECT cant_cuotas, cuotas_pagas FROM deudas WHERE id_alumno = '$alumno' AND id_curso = '$curso'";
    $result = mysqli_query($conn, $sql_obtener_cant_cuotas);
    $row = mysqli_fetch_assoc($result);
    $cantCuotas = intval($row['cant_cuotas']);
    $cuotasPagas = intval($row['cuotas_pagas']);
    
    //Si todas las cuotas se pagaron y el curso ya termino, se elimina la deuda de la tabla, si no simplemente se actualizan los valores
    if (($cuotasPagas == $cantCuotas)) {
        // Ejecutar una consulta de eliminación en lugar de actualización
        $sql_eliminar_deuda = "DELETE FROM deudas WHERE id_alumno = '$alumno' AND id_curso = '$curso'";
        if (!mysqli_query($conn, $sql_eliminar_deuda)) {
            $success = false;
            echo "Error al eliminar cuota en deudas: " . mysqli_error($conn);
            break; // Sal del bucle si hay un error
        }
    }
        
    // Insertar el pago en la tabla cobros
    $sql_cobros = "INSERT INTO cobros (id_alumno, id_curso, fecha_cobro, monto, n_cuota, id_descuento, descuento) VALUES ('$alumno', '$curso', '$fecha', '$monto', '$numCuota','$id_descu', '$descuento')";   
    if (!mysqli_query($conn, $sql_cobros)) {
        $success = false; 
        echo "Error al insertar en cobros: " . mysqli_error($conn);
        break; // Sal del bucle si hay un error
    }
}


if ($success) {
    // Confirma la transacción
    mysqli_commit($conn);
    echo json_encode(["message" => "Operaciones realizadas exitosamente"]);
} else {
    // Revierte la transacción en caso de error
    mysqli_rollback($conn);
    echo json_encode(["error" => "Error en una o más operaciones"]);
}

// Habilita la autocommit nuevamente
mysqli_autocommit($conn, true);

// Cierra la conexión
$conn->close();
?>