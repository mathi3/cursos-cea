<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Recibe los datos JSON enviados desde JavaScript
$jsonData = file_get_contents("php://input");

// Decodifica el JSON en un array asociativo de PHP
$data = json_decode($jsonData, true);

if ($data === null) {
    // Si la decodificación falla, respondemos con un error
    echo json_encode(array("error" => "Error al decodificar los datos JSON"));
} else {
    // Puedes acceder a los datos como un array asociativo
    $nombre = $data['nombre'];
    $materia = $data['materia'];
    $id_profesor = $data['profesor'];
    $fecha_inicio = $data['fecha_inicio'];
    $dia = $data['dia'];
    $horario = $data['horario'];
    $id = $data['id'];
    // Consulta a la base de datos para obtener la materia segun su ID
    $sql = "UPDATE cursos
            SET 
            descripcion_curso = '$nombre', -- Reemplaza 'Nueva descripción' con el valor que desees
            id_materia = '$materia',
            id_profesor = '$id_profesor',
            fecha_inicio = '$fecha_inicio', -- Reemplaza 'Nueva fecha' con el valor que desees
            horario = '$horario', -- Reemplaza 'Nuevo horario' con el valor que desees
            dia = '$dia' -- Reemplaza 'Nuevo día' con el valor que desees
    WHERE id_curso = ".$id;

    // Se envia el resultado de la consulta
    if (mysqli_query($conn, $sql)) {
        echo json_encode(array("success" => "Proceso completado correctamente"));
    } else {
        echo json_encode(array("error" => "Error en el proceso"));
    }
}

$conn->close();

?>
