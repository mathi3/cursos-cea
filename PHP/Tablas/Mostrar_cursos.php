<?php
error_reporting(E_ALL);
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Consulta a la base de datos para obtener los cursos con los datos de la materia y el profesor
$sql = "SELECT 
    cursos.id_curso, 
    cursos.descripcion_curso, 
    cursos.horario, 
    cursos.dia, 
    cursos.activo,
    materias.nombre_materia AS materia,
    CONCAT(profesores.nombre, ' ', profesores.apellido) AS profesor,
    COUNT(deudas.id_curso) AS inscriptos
FROM cursos
INNER JOIN profesores ON cursos.id_profesor = profesores.id_profesor
INNER JOIN materias ON cursos.id_materia = materias.id_materia
LEFT JOIN deudas ON cursos.id_curso = deudas.id_curso
GROUP BY cursos.id_curso, cursos.descripcion_curso, cursos.horario, cursos.dia, cursos.activo, materias.nombre_materia, CONCAT(profesores.nombre, ' ', profesores.apellido)
";
$result = $conn->query($sql);


// Obtener los datos de los cursos en formato JSON
$cursos = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $cursos[] = $row;
    }
}

$conn->close();

// Enviar los datos de los cursos en formato JSON
header('Content-Type: application/json');
echo json_encode($cursos);
?>
