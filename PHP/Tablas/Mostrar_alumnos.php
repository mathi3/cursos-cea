<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Consulta a la base de datos para obtener los alumnos
$sql = "SELECT id_alumno, nombre, apellido, dni, fecha_inscripcion, empresa, fecha_nacimiento, direccion, codigo_postal, telefono, email FROM alumnos";
$result = $conn->query($sql);

// Obtener los datos de los alumnos en formato JSON
$alumnos = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $alumnos[] = $row;
    }
}


$conn->close();

// Enviar los datos de los alumnos en formato JSON
header('Content-Type: application/json');
echo json_encode($alumnos);
?>
