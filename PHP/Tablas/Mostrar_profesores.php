<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
} else {
    $sql = "SELECT id_profesor, nombre, apellido, fecha_contratacion, dni, titulo, direccion, cod_postal, telefono, email, fecha_nacimiento FROM profesores";
    $result = $conn->query($sql);

    
// Obtener los datos de los profesores en formato JSON
$profesores = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $profesores[] = $row;
    }
} else {
        die("Error en la consulta SQL: " . $conn->error);
    }
}

$conn->close();

// Enviar los datos de los profesores en formato JSON
header('Content-Type: application/json');
echo json_encode($profesores);
?>
