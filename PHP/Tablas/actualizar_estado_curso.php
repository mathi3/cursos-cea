<?php
// Verificar si se reciben los datos esperados
if (isset($_POST['cursoId']) && isset($_POST['nuevoValor'])) {
    $cursoId = $_POST['cursoId'];
    $nuevoValor = $_POST['nuevoValor'];
    $fun = $_POST['fun'];

    // Conexión a la base de datos
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "cea_base_de_datos";

    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Error de conexión a la base de datos: " . $conn->connect_error);
    }

    if($fun==0){
        $fecha = $_POST['fechaIni'];
        $fechaFormateada = date('Y-m-d', strtotime($fecha));
  

        /*// Preparar la declaración SQL para actualizar el valor
        $sql = "UPDATE cursos SET activo = ?, fecha_inicio = ? WHERE id_curso = ?";
        // Preparar y ejecutar la consulta con una declaración preparada
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("si", $nuevoValor, "2020-12-12", $cursoId);*/    
        $sql = "UPDATE cursos SET activo = '$nuevoValor', fecha_inicio = '$fecha' WHERE id_curso = '$cursoId'";
        echo $sql;
        $resultado = mysqli_query($conn, $sql);
        echo $resultado;

    } else {
        // Preparar la declaración SQL para actualizar el valor
        $sql = "UPDATE cursos SET activo = ? WHERE id_curso = ?";
        // Preparar y ejecutar la consulta con una declaración preparada
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("si", $nuevoValor, $cursoId);   
    }
    
    /*if ($stmt->execute()) {
        echo "Valor actualizado en la base de datos";
    } else {
        echo "Error al actualizar el valor en la base de datos: " . $stmt->error;
    }*/

    // Cerrar la conexión
    $conn->close();
} else {
    echo "Faltan datos o datos incorrectos.";
}
?>