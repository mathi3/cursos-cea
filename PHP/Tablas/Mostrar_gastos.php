<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}
// Consulta a la base de datos para obtener los cobros correspondientes al dia actual
$sql = "SELECT * FROM gastos WHERE DATE(fecha_gasto) = CURDATE()";

$result = $conn->query($sql);

// Obtener los datos de los gastos en formato JSON
$gastos = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $gastos[] = $row;
    }
}

$conn->close();

// Enviar los datos de los gastos en formato JSON
header('Content-Type: application/json');
echo json_encode($gastos);
?>
