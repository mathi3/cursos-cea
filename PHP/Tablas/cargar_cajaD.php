<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Consulta a la base de datos para verificar si ya hay una carga de la caja en el dia de hoy
$sql = "SELECT apertura FROM caja_diaria WHERE DATE(fecha_caja) = CURDATE()";

$result=$conn->query($sql);

// Verificar si se obtuvieron resultados y enviarlos como respuesta JSON
if ($result->num_rows > 0) {
    $apertura=mysqli_fetch_assoc($result);
    // Enviar el monto de apertura en formato JSON
    
    header('Content-Type: application/json');
    echo json_encode($apertura);
} else {
    echo json_encode(array(
        "error" => "No se ha encontrado un monto de apertura"
    ));
}

$conn->close();

?>
