<?php
// Conexion a la base de datos
$host = "localhost"; // nombre del servidor de base de datos
$usuario = "root"; // nombre de usuario para acceder a la base de datos
$contrasena = ""; // contrasena para acceder a la base de datos
$dbname= "cea_base_de_datos"; // nombre de la base de datos

// Conexion a la base de datos
$conn = mysqli_connect($host, $usuario, $contrasena, $dbname);

// Verificar si la conexión es exitosa
if (!$conn) {
    die("Error de conexión: " . mysqli_connect_error());
}

$idAlumno = $_POST['idAlumno'];
$fun = $_POST['fun'];

if ($fun == 0) {
    // Realizar la consulta para obtener los datos de cobro
    $sql = "SELECT co.fecha_cobro, cu.descripcion_curso as curso, co.monto, co.n_cuota 
    FROM cobros as co
    INNER JOIN cursos as cu ON co.id_curso = cu.id_curso
    WHERE id_alumno = '$idAlumno'";

    $resultado = mysqli_query($conn, $sql);
    $datos = array();

    if (mysqli_num_rows($resultado) > 0) {
        while ($row = mysqli_fetch_assoc($resultado)) {
            $datos[] = $row;
        }
    }

    // Enviar los datos en formato JSON
    echo json_encode($datos);
} else if ($fun == 1) { // Debes usar "else if" en lugar de "else" para verificar $fun
    // Realizar la consulta para obtener la fecha de inscripción
    $sql = "SELECT fecha_inscripcion FROM alumnos WHERE id_alumno = '$idAlumno'";

    $resultado = mysqli_query($conn, $sql);

    if (mysqli_num_rows($resultado) > 0) {
        $row = mysqli_fetch_assoc($resultado);
        $fechaInscripcion = $row['fecha_inscripcion'];
        // Enviar la fecha de inscripción como respuesta
        echo json_encode(array('fecha_inscripcion' => $fechaInscripcion)); // Devolver la fecha en formato JSON
    } else {
        // No se encontraron datos
        echo "No se encontraron datos";
    }
} else if ($fun == 2){
        // Realizar la consulta para obtener los datos de cobro
    $sql = "SELECT 
                c.descripcion_curso,
                d.cant_cuotas - d.cuotas_pagas AS cuotas_pendientes,
                d.valor,
                (d.cant_cuotas - d.cuotas_pagas) * d.valor AS total_deuda
            FROM deudas as d
            INNER JOIN cursos as c ON d.id_curso = c.id_curso
            WHERE id_alumno = $idAlumno -- Reemplaza '1' con el ID del alumno que desees consultar
            GROUP BY d.id_curso";

    $resultado = mysqli_query($conn, $sql);

    if (mysqli_num_rows($resultado) > 0) {
    while ($row = mysqli_fetch_assoc($resultado)) {
    $datos[] = $row;
    }
    }

    // Enviar los datos en formato JSON
    echo json_encode($datos);

}

// Cerrar la conexión
mysqli_close($conn);
?>
