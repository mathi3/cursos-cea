<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

$apertura = intval($_POST["montoIni"]);

// Verifico si ya se cargo el dia de hoy
$sql_verificar = "SELECT COUNT(*) as count FROM caja_diaria WHERE fecha_caja = CURDATE()";
$result_verificar = $conn->query($sql_verificar);
$fila_verificar = $result_verificar->fetch_assoc();

// --- VERSION ANTIGUA, MAS LINEAS DE CODIGO ---
/*
  $sql="";  
  if($fila_verificar["count"] > 0){
    //Si existe modificamos el valor de la apertura
    $sql = "UPDATE caja_diaria SET apertura = '$apertura' WHERE fecha_caja = CURDATE()";
    $conn->query($sql);
} else {
    // Si no existe, realizamos una inserción
    $sql = "INSERT INTO caja_diaria (fecha_caja, apertura) VALUES (CURDATE(), '$apertura')";
    $conn->query($sql);
}*/

//Se inserta la fecha y la apertura en la tabla de caja diarias en sus columnas correspondientes, si ya existe (duplicacion de insercion), se actualiza el valor de la apertura.
$sql = "INSERT INTO caja_diaria (fecha_caja, apertura) VALUES (CURDATE(), '$apertura') 
ON DUPLICATE KEY UPDATE apertura = '$apertura'";

if (mysqli_query($conn, $sql)) {
    echo json_encode(array("success" => "Proceso completado correctamente"));
} else {
    echo json_encode(array("error" => "Error en el proceso"));
}
$conn->close();

?>
