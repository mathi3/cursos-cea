<?php
// Al comienzo del script PHP
header('Content-Type: application/json');

// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Consulta a la base de datos para obtener los pagos realizados por los alumnos
$sql = "SELECT p.id_cobro,
a.nombre,
a.apellido,
a.dni,
c.descripcion_curso,
MAX(p.fecha_cobro) AS fecha_cobro,
p.n_cuota,
MAX(p.monto) AS monto,
d.descripcion AS tipo_pago
FROM cobros AS p
INNER JOIN alumnos AS a ON p.id_alumno = a.id_alumno
INNER JOIN cursos AS c ON p.id_curso = c.id_curso
INNER JOIN descuentos AS d ON p.id_descuento = d.id_descuento
WHERE DATE(p.fecha_cobro) = CURDATE()
GROUP BY p.id_cobro, a.nombre, a.apellido, a.dni, c.descripcion_curso;";

/*"SELECT 
co.id_cobro,
al.nombre,
al.apellido,
al.dni,
cu.descripcion_curso,
co.fecha_cobro,
co.n_cuota,
co.monto,
co.tipo_pago
FROM cobros AS co 
INNER JOIN alumnos AS al ON co.id_alumno = al.id_alumno
INNER JOIN cursos AS cu ON co.id_curso = cu.id_curso;
";*/


$result = $conn->query($sql);

// Verificar si hay un error en la consulta
if ($result === false) {
    // Registrar el error en la consola
    error_log("Error en la consulta SQL: " . $conn->error);

    die("Error en la consulta SQL: " . $conn->error);
}

// Obtener los datos de los pagos en formato JSON
$pagos = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $pagos[] = $row;
    }
}

$conn->close();

// Enviar los datos de los pagos en formato JSON
header('Content-Type: application/json');
echo json_encode($pagos);
?>