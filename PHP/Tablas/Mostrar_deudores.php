<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}
// Consulta que obtiene la cantidad de cursos, el total de deuda, total de cuotas a pagar y total de cuotas pagas de todos los cursos en los que esta inscripto cada alumno
$sql = "SELECT 
            s.id_alumno,
            a.nombre,
            a.apellido,
            c.fecha_cobro AS ultimo_cobro,
            COUNT(s.id_curso) AS cant_cursos,
            SUM(s.cuotas_pagas) AS cuotas_pagas, 
            SUM(s.cant_cuotas) AS cant_cuotas,
            d.deuda_total
        FROM 
            deudas AS s 
            INNER JOIN alumnos AS a ON s.id_alumno = a.id_alumno
            LEFT JOIN (
                SELECT id_alumno, MAX(fecha_cobro) AS fecha_cobro
                FROM cobros
                GROUP BY id_alumno
                HAVING fecha_cobro = MAX(fecha_cobro)
            ) AS c ON s.id_alumno = c.id_alumno
            LEFT JOIN (
                SELECT id_alumno, SUM(valor * (cant_cuotas - cuotas_pagas)) AS deuda_total
                FROM (
                    SELECT id_alumno, valor, cant_cuotas, cuotas_pagas, (valor * (cant_cuotas - cuotas_pagas)) AS suma_valores
                    FROM deudas
                ) AS subconsulta
                GROUP BY id_alumno
            ) AS d ON s.id_alumno = d.id_alumno
            GROUP BY s.id_alumno
";

$result = $conn->query($sql);
/* 
EJEMPLO DE SALIDA DE ESTA QUERY
id_alumno	nombre	apellido	ultimo_cobro	cant_cursos	    cuotas_pagas    cant_cuotas	    deuda_total	
1           Maximo  Domiguez    2023-04-11              2                   0        28              39500
5           valentin Gluzszczuk 2023-06-15              1                   0        10              13000
*/

// Obtener los datos de los pagos en formato JSON
$pagos = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $pagos[] = $row;
    }
}

$conn->close();

// Enviar los datos de los pagos en formato JSON
header('Content-Type: application/json');
echo json_encode($pagos);
?>