<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}
//Almaceno la id de la materia seleccionada
$id=$_POST['id'];

// Obtener los valores del formulario
$nombre_materia = $_POST['nombre_materia'];
$cuotas = $_POST['cuotas'];
$valor = $_POST['valor'];
$valor_bonif = $_POST['valor_bonif'];
$matricula = $_POST['matricula'];
$descripcion = $_POST['descripcion'];

// Consulta a la base de datos para obtener la materia segun su ID
$sql = "UPDATE materias SET nombre_materia = '".$nombre_materia."', cuotas = ".$cuotas.", valor = ".$valor.", valor2 = ".$valor_bonif.", descripcion = '".$descripcion."' WHERE id_materia = ".$id;

// Se envia el resultado de la consulta
if (mysqli_query($conn, $sql)) {
    echo json_encode(array("success" => "Proceso completado correctamente"));
} else {
    echo json_encode(array("error" => "Error en el proceso"));
}

$conn->close();

?>
