<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}
//Almaceno la id de la materia seleccionada
$id=$_POST['id'];

// Consulta a la base de datos para obtener la materia segun su ID
$sql = "SELECT * FROM profesores WHERE id_profesor=".$id;
$result = $conn->query($sql);

// Obtener los datos de las materias en formato JSON  OPTIMIZAR ESTO
if ($result->num_rows > 0) {
    $profesor=mysqli_fetch_assoc($result);
    // Enviar los datos de las materias en formato JSON
    header('Content-Type: application/json');
    echo json_encode($profesor);
} else {
    echo json_encode(array(
        "error" => "No se ha encontrado la materia"
    ));
}

$conn->close();

?>
