<?php
// Conexion a la base de datos
$host = "localhost"; // nombre del servidor de base de datos
$usuario = "root"; // nombre de usuario para acceder a la base de datos
$contrasena = ""; // contrasena para acceder a la base de datos
$dbname= "cea_base_de_datos"; // nombre de la base de datos

// Conexion a la base de datos
$conn = mysqli_connect($host, $usuario, $contrasena, $dbname);

// Verificar si la conexión es exitosa
if (!$conn) {
    die("Error de conexión: " . mysqli_connect_error());
}

// Obtener los valores del formulario
$dni = $_POST['dni'];
$nombre = $_POST['nombre'];
$apellido = $_POST['apellido'];
$fecha_inscripcion = $_POST['fecha-inscripcion'];
$empresa = $_POST['empresa'];
$fecha_nacimiento = $_POST['fecha-nacimiento'];
$direccion = $_POST['direccion'];
$codigo_postal = $_POST['codigo-postal'];
$email = $_POST['email'];
$telefono = $_POST['telefono'];
$observacion = $_POST['observacion'];
$datos_padres = $_POST['datos-padres'];

// Insertar los datos en la base de datos
$sql = "INSERT INTO alumnos (dni, nombre, apellido, fecha_inscripcion, empresa, fecha_nacimiento, direccion, num_direccion, codigo_postal, email, telefono, observacion, datos_padres)
        VALUES ('$dni', '$nombre', '$apellido', '$fecha_inscripcion', '$empresa', '$fecha_nacimiento', '$direccion', '$num_direccion', '$codigo_postal', '$email', '$telefono', '$observacion', '$datos_padres')";


if (mysqli_query($conn, $sql)) {
    echo "Registro insertado correctamente en la base de datos";
} else {
    echo "Error al insertar el registro: " . mysqli_error($conn);
}

// Cerrar la conexión
mysqli_close($conn);
?>
