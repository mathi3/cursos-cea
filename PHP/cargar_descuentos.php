<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Consulta a la base de datos para obtener la materia segun su ID
$sql = "SELECT * FROM descuentos";
$result = $conn->query($sql);

// Verificar si se obtuvieron resultados y enviarlos como respuesta JSON
if ($result->num_rows > 0) {
    $descuentos = array(); // Arreglo para almacenar los descuentos

    while ($row = $result->fetch_assoc()) {
        $descuentos[] = $row; // Agregar cada fila al arreglo de descuentos
    }
    
    header('Content-Type: application/json');
    echo json_encode($descuentos);
} else {
    echo json_encode(array(
        "error" => "No se ha encontrado un monto de apertura"
    ));
}

$conn->close();

?>