<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Obtener el id de la materia desde el parámetro GET o POST
$id = $_REQUEST['id'];

// Validar y escapar el id de la materia
if (is_numeric($id)) {
    $id = $conn->real_escape_string($id);
} else {
    die("Error: el id del profesor debe ser un número");
}

// Consulta preparada para eliminar la materia si existe
$sql = "DELETE FROM profesores WHERE id_profesor = ? AND EXISTS (SELECT * FROM profesores WHERE id_profesor = ?)";
$stmt = $conn->prepare($sql);
$stmt->bind_param("ii", $id, $id);
$deleteResult = $stmt->execute();

if ($deleteResult) {
    if ($stmt->affected_rows > 0) {
        echo json_encode(array("success" => "Profesor eliminada exitosamente"));
    } else {
        echo json_encode(array("error" => "No se ha encontrado el profesor"));
    }
} else {
    echo json_encode(array("error" => "Error al eliminar el profesor"));
}

$stmt->close();
$conn->close();

?>