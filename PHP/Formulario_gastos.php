<?php
// Configurar la conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar la conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Obtener los datos del formulario y escaparlos
$nombre = mysqli_real_escape_string($conn, $_POST['nombre']);
$categoria = mysqli_real_escape_string($conn, $_POST['categoria']);
$fecha = mysqli_real_escape_string($conn, $_POST['fecha']);
$monto = floatval($_POST['monto']); // Asegurarse de que el monto sea un número

// Verificar la caja diaria
$fecha = date("Y-m-d"); // Obtén la fecha actual en el formato necesario
$sql_verificacion = "INSERT IGNORE INTO caja_diaria (fecha_caja, apertura) VALUES ('$fecha', '0')";
$success = true;

if (!mysqli_query($conn, $sql_verificacion)) {
    $success = false;
    echo "Error al verificar la caja diaria: " . mysqli_error($conn);
}

if ($success) {
    // Insertar el pago en la tabla pagos
    $sql = "INSERT INTO gastos (nombre_gasto, categoria, fecha_gasto, monto_gasto) VALUES ('$nombre', '$categoria', '$fecha', '$monto')";
    
    if ($conn->query($sql) === TRUE) {
        echo "Pago registrado correctamente.";
    } else {
        echo "Error al registrar el pago: " . $conn->error;
    }
}

$conn->close();
?>
