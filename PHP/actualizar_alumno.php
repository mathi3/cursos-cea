<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Almaceno la ID del alumno seleccionado
$id = $_POST['id'];

// Obtener los valores del formulario y escaparlos para evitar inyección SQL
$nombre = mysqli_real_escape_string($conn, $_POST['nombre']);
$apellido = mysqli_real_escape_string($conn, $_POST['apellido']);
$dni = mysqli_real_escape_string($conn, $_POST['dni']);
$fecha_inscripcion = mysqli_real_escape_string($conn, $_POST['fecha_inscripcion']);
$empresa = mysqli_real_escape_string($conn, $_POST['empresa']);
$fecha_nacimiento = mysqli_real_escape_string($conn, $_POST['fecha_nacimiento']);
$direccion = mysqli_real_escape_string($conn, $_POST['direccion']);
$codigo_postal = mysqli_real_escape_string($conn, $_POST['codigo_postal']);
$telefono = mysqli_real_escape_string($conn, $_POST['telefono']);
$email = mysqli_real_escape_string($conn, $_POST['email']);
$datos_padres = mysqli_real_escape_string($conn, $_POST['datos_padres']);
$observacion = mysqli_real_escape_string($conn, $_POST['observacion']);

// Consulta a la base de datos para actualizar el alumno según su ID
$sql = "UPDATE alumnos SET 
            nombre = '".$nombre."', 
            apellido = '".$apellido."', 
            dni = '".$dni."', 
            fecha_inscripcion = '".$fecha_inscripcion."', 
            empresa = '".$empresa."',
            fecha_nacimiento = '".$fecha_nacimiento."',
            direccion = '".$direccion."',
            codigo_postal = '".$codigo_postal."',
            telefono = '".$telefono."',
            email = '".$email."',
            datos_padres = '".$datos_padres."',
            observacion = '".$observacion."'
            WHERE id_alumno = ".$id; 

// Se envía el resultado de la consulta
if ($conn->query($sql) === TRUE) {
    echo json_encode(array("success" => "Proceso completado correctamente"));
} else {
    echo json_encode(array("error" => "Error en el proceso: " . $conn->error));
}

$conn->close();
?>
