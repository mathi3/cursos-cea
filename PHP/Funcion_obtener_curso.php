<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Inicializar la variable $id_alumno
$id_alumno = null;

// Comprobar si se envió 'alumno_id' a través de POST o GET
if (isset($_POST['alumno_id'])) {
    $id_alumno = $_POST['alumno_id'];
} elseif (isset($_GET['alumno_id'])) {
    $id_alumno = $_GET['alumno_id'];
}

// Verificar si $id_alumno no es nulo
if ($id_alumno !== null) {
    // Consulta SQL para obtener la descripción de los cursos vinculados al ID del alumno en la tabla deudas
    $sql = "SELECT d.id_curso, c.descripcion_curso
            FROM cursos c
            INNER JOIN deudas d ON c.id_curso = d.id_curso
            WHERE d.id_alumno = $id_alumno AND d.cuotas_pagas < d.cant_cuotas"; //Verifico de traer solo los cursos que todavia tienen cuotas pendientes por pagar

    $result = $conn->query($sql);
    // Verificar si se obtuvieron resultados
    if ($result->num_rows > 0) {
        // Crear un array para almacenar las descripciones de los cursos
        $cursos = array();

        // Recorrer los resultados y agregar las descripciones al array
        while ($row = $result->fetch_assoc()) {
            $cursos[] = $row;
        }

        // Devolver el array de descripciones de cursos como respuesta JSON
        header("Content-Type: application/json");
        echo json_encode($cursos);

    } else {
        echo "No se encontraron cursos para este alumno.";
    }
} else {
    // Consulta SQL para obtener la descripción de los cursos vinculados al ID del alumno en la tabla deudas
    $sql = "SELECT cursos.id_curso, cursos.descripcion_curso FROM cursos";
$result = $conn->query($sql);

// Verificar si se obtuvieron resultados
if ($result->num_rows > 0) {
    // Crear un array para almacenar los datos de los cursos
    $cursos = array();

    // Recorrer los resultados y agregar los datos al array
    while ($row = $result->fetch_assoc()) {
        $cursos[] = $row;
    }

    // Devolver el array como respuesta JSON
    header("Content-Type: application/json");
    echo json_encode($cursos);
} else {
    echo "No se encontraron cursos.";
}
}

// Cerrar la conexión a la base de datos
$conn->close();
?>
