<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Obtener el id de la materia desde el parámetro GET o POST
$id = $_REQUEST['id'];

// Validar y escapar el id de la materia
if (is_numeric($id)) {
    $id = $conn->real_escape_string($id);
} else {
    die("Error: el id de la materia debe ser un número");
}

// Consulta preparada para eliminar la materia si existe
$sql = "DELETE FROM materias WHERE id_materia = ? AND EXISTS (SELECT * FROM materias WHERE id_materia = ?)";
$stmt = $conn->prepare($sql);
$stmt->bind_param("ii", $id, $id);
$deleteResult = $stmt->execute();

if ($deleteResult) {
    if ($stmt->affected_rows > 0) {
        echo json_encode(array("success" => "Materia eliminada exitosamente"));
    } else {
        echo json_encode(array("error" => "No se ha encontrado la materia"));
    }
} else {
    echo json_encode(array("error" => "Error al eliminar la materia"));
}

$stmt->close();
$conn->close();

?>