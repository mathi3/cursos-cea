<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Inicializar las variables $id_alumno y $id_curso
$id_alumno = null;
$id_curso = null;

// Comprobar si se enviaron 'alumno_id' y 'curso_id' a través de POST o GET
if (isset($_POST['alumno_id']) && isset($_POST['curso_id'])) {
    $id_alumno = $_POST['alumno_id'];
    $id_curso = $_POST['curso_id'];
} elseif (isset($_GET['alumno_id']) && isset($_GET['curso_id'])) {
    $id_alumno = $_GET['alumno_id'];
    $id_curso = $_GET['curso_id'];
}

// Verificar si $id_alumno y $id_curso no son nulos
if ($id_alumno !== null && $id_curso !== null) {
    // Consulta SQL para obtener los datos de la tabla deudas relacionados con el alumno y el curso especificados
    $sql = "SELECT * FROM deudas WHERE id_alumno = $id_alumno AND id_curso = $id_curso";

    $result = $conn->query($sql);
    // Verificar si se obtuvieron resultados
    if ($result->num_rows > 0) {
        // Crear un array para almacenar los datos de deudas
        $deudas = array();

        // Consulta SQL para obtener la fecha de inicio del curso
        $sql_obtener_fecha_inicio = "SELECT fecha_inicio FROM cursos WHERE id_curso = $id_curso";
        $result2 = $conn->query($sql_obtener_fecha_inicio);
        $row2 = $result2->fetch_assoc();
        $fecha_inicio = $row2['fecha_inicio'];
            
        // Recorrer los resultados y agregar los datos al array
    while ($row = $result->fetch_assoc()) {
        // Agregar la fecha de inicio al array de cada deuda
        $row['fecha_inicio'] = $fecha_inicio;
        $deudas[] = $row;
    }

        // Devolver el array de datos de deudas como respuesta JSON
        header("Content-Type: application/json");
        echo json_encode($deudas);
    } else {
        echo "No se encontraron deudas para este alumno y curso.";
    }
} else {
    echo json_encode(array("error" => "No se proporcionaron ID de alumno y curso válidos."));
}

// Cerrar la conexión a la base de datos
$conn->close();
?>
