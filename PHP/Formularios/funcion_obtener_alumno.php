<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Error en la conexión a la base de datos: " . $conn->connect_error);
}

$sql = "SELECT id_alumno, nombre, apellido FROM alumnos";
$result = $conn->query($sql);

// Verificar si se obtuvieron resultados
if ($result->num_rows > 0) {
    // Crear un array para almacenar los datos de los alumnos
    $alumnos = array();

    // Recorrer los resultados y agregar los datos al array
    while ($row = $result->fetch_assoc()) {
        $alumnos[] = $row;
    }

    // Devolver el array como respuesta JSON
    header("Content-Type: application/json");
    echo json_encode($alumnos);
} else {
    echo "No se encontraron alumnos.";
}

// Cerrar la conexión a la base de datos
$conn->close();
?>
