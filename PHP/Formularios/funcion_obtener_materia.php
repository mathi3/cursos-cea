<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Error en la conexión a la base de datos: " . $conn->connect_error);
}

$sql = "SELECT id_materia, nombre_materia FROM materias";
$result = $conn->query($sql);

// Verificar si se obtuvieron resultados
if ($result->num_rows > 0) {
    // Crear un array para almacenar los datos de las materias
    $materias = array();

    // Recorrer los resultados y agregar los datos al array
    while ($row = $result->fetch_assoc()) {
        $materias[] = $row;
    }

    // Convertir el array a formato JSON
    $jsonMaterias = json_encode($materias);

    // Enviar el JSON como respuesta
    echo $jsonMaterias;
} else {
    echo "No se encontraron materias.";
}

// Cerrar la conexión
$conn->close();
?>
