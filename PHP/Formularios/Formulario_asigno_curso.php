<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Obtener los datos del formulario (realiza la sanitización si es necesario)
$alumno = $_REQUEST['alumno'];
$curso = $_REQUEST['curso'];
$beca = $_REQUEST['beca'];

// Consulta SQL para contar las filas que coinciden con el valor
$sql = "SELECT COUNT(*) as count FROM deudas WHERE id_curso = ? AND id_alumno = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("ss", $curso, $alumno);
$stmt->execute();
$resultado_verificacion = $stmt->get_result();
$stmt->close();

$response = array();

if ($resultado_verificacion) {
    $fila = mysqli_fetch_assoc($resultado_verificacion);
    $cantidad_filas = $fila['count'];

    // Verificar si hay al menos una fila con el valor
    if ($cantidad_filas >= 1) {
        // Valor ya existe, enviar una respuesta JSON con un mensaje de error
        $response['success'] = false;
        $response['message'] = 'El valor ya existe en la base de datos.';
        header('Content-Type: application/json');
        die(json_encode($response));
    } else {
        // Consulta SQL para obtener la id_materia según el nombre del curso
        $sql_id_materia = "SELECT id_materia FROM cursos WHERE id_curso = ?";
        $stmt = $conn->prepare($sql_id_materia);
        $stmt->bind_param("s", $curso);
        $stmt->execute();
        $resultado_id_materia = $stmt->get_result();
        $stmt->close();

        // Extraer la id_materia de la consulta
        $fila_id_materia = mysqli_fetch_assoc($resultado_id_materia);
        $id_m = $fila_id_materia['id_materia'];

        //Consulta para traer los valores de la materia y la cantidad de cuotas.
        $sql_precios = "SELECT materias.valor, materias.valor2, materias.cuotas FROM cursos
        INNER JOIN materias ON cursos.id_materia = materias.id_materia
        WHERE cursos.id_materia = ?";
        $stmt = $conn->prepare($sql_precios);
        $stmt->bind_param("s", $id_m);
        $stmt->execute();
        $resultado_precios = $stmt->get_result();
        $stmt->close();

        // Extraer los valores en variables
        $fila_precios = mysqli_fetch_assoc($resultado_precios);
        $valor = $fila_precios['valor'];
        $valor2 = $fila_precios['valor2'];
        $cuotas = $fila_precios['cuotas'];

        //Realizamos el cálculo del valor becado si es necesario
        if ($beca != 0) {
            $valor = round($valor - (floatval($valor) * floatval($beca))); //Nos aseguramos de que los valores sean numéricos
            $valor2 = round($valor2 - (floatval($valor2) * floatval($beca)));
        }
        /* !!!!!!!! SE REDONDEAN LOS VALORES PORQUE LA BASE DE DATOS ESTA EN ENTERO, CAMBIAR A FUTURO !!!!!!!! */

        // Insertar la asignación en la tabla deudas
        $sql_insert = "INSERT INTO deudas (id_alumno, id_curso, valor, valor2, cant_cuotas) VALUES (?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql_insert);
        $stmt->bind_param("ssddd", $alumno, $curso, $valor, $valor2, $cuotas);

        if ($stmt->execute()) {
            $response['success'] = true;
            $response['message'] = "Asignación guardada correctamente.";

        } else {
            $response['success'] = false;
            $response['message'] = "Error al guardar la asignación: " . $stmt->error;

        }

        $stmt->close();
        header('Content-Type: application/json');
        echo json_encode($response);

    }
}

?>