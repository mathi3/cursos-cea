<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Error en la conexión a la base de datos: " . $conn->connect_error);
}

$sql = "SELECT id_profesor, CONCAT(nombre, ' ', apellido) as nombre FROM profesores";
$result = $conn->query($sql);

// Verificar si se obtuvieron resultados
if ($result->num_rows > 0) {
    // Crear un array para almacenar los datos de los profesores
    $profesores = array();

    // Recorrer los resultados y agregar los datos al array
    while ($row = $result->fetch_assoc()) {
        $profesores[] = $row;
    }

    // Convertir el array a formato JSON
    $jsonProfesores = json_encode($profesores);

    // Enviar el JSON como respuesta
    echo $jsonProfesores;
} else {
    echo "No se encontraron profesores.";
}

// Cerrar la conexión
$conn->close();
?>
