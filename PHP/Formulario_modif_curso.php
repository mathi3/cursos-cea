<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}
//Almaceno la id del curso seleccionado
$id=$_POST['id'];

// Consulta a la base de datos para obtener el curso segun su ID
$sql = "SELECT c.descripcion_curso,
m.id_materia, 
m.nombre_materia AS materia, 
p.id_profesor,
CONCAT(p.nombre, ' ', p.apellido) as nombre,
c.fecha_inicio, 
c.horario, 
c.dia
FROM cursos AS c
INNER JOIN materias AS m ON c.id_materia = m.id_materia
INNER JOIN profesores AS p ON c.id_profesor = p.id_profesor
WHERE c.id_curso =".$id;
$result = $conn->query($sql);

// Obtener los datos de los cursos en formato JSON
if ($result->num_rows > 0) {
    $materia=mysqli_fetch_assoc($result);
    // Enviar los datos de los cursos en formato JSON
    header('Content-Type: application/json');
    echo json_encode($materia);
} else {
    echo json_encode(array(
        "error" => "No se ha encontrado el curso"
    ));
}

$conn->close();

?>
