-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2023 a las 15:06:39
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cea_base_de_datos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `id_alumno` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `dni` int(10) NOT NULL,
  `fecha_inscripcion` date NOT NULL,
  `empresa` varchar(70) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `num_direccion` int(20) NOT NULL,
  `codigo_postal` int(6) NOT NULL,
  `telefono` int(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `foto` blob NOT NULL,
  `datos_padres` varchar(100) NOT NULL,
  `observacion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id_alumno`, `nombre`, `apellido`, `dni`, `fecha_inscripcion`, `empresa`, `fecha_nacimiento`, `direccion`, `num_direccion`, `codigo_postal`, `telefono`, `email`, `foto`, `datos_padres`, `observacion`) VALUES
(1, 'Maximo', 'Domiguez', 46789456, '2023-04-11', '', '2004-07-27', 'balestrini', 1234, 1838, 1145789636, 'lautarodomiguez@gmail.com', '', '', ''),
(4, 'Selene', 'ygyhyhj', 515651, '2023-06-12', 'frges', '2023-05-30', 'vgrfsh', 561261, 5614, 15656, 'asfsbhed vgf@mf', '', 'rfgsreh', 'fgse'),
(5, 'valentin', 'Gluzszczuk', 345345654, '2023-06-15', 'CEA', '2005-06-24', 'm.e', 321, 1254, 1123546544, 'df@vgf', '', '', 'valentinnnnnn'),
(6, 'ernesto', 'lopez', 231324235, '2023-06-29', 'coca-cola', '1991-08-19', 'Av. Pedrera', 1546, 1452, 115367865, 'lopez234534@gmail.com.ar', '', '0', 'alumno con dificultad en matematicas'),
(7, 'prueba', 'prueba', 4598784, '2023-07-27', 'prueba', '2004-07-27', 'prueba ', 2559, 1832, 1132147092, 'maximolautaro04@gmail.com', '', '', 'pruebaa'),
(10, 'juanchito', 'Pérezeso', 12323, '2023-11-11', 'edede', '1999-11-11', 'Calle Principal 13', 0, 545, 12, 'juan@example.ar', '', 'María Pérez y Pedro edede', 'dede');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencias`
--

CREATE TABLE `asistencias` (
  `id_asistencia` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `fecha_asistencia` date NOT NULL,
  `estado_asistencia` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_diaria`
--

CREATE TABLE `caja_diaria` (
  `fecha_caja` date NOT NULL,
  `apertura` int(100) NOT NULL,
  `cierre` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `caja_diaria`
--

INSERT INTO `caja_diaria` (`fecha_caja`, `apertura`, `cierre`) VALUES
('2023-09-19', 500, 0),
('2023-09-21', 500, 0),
('2023-09-28', 500, 0),
('2023-10-11', 0, 0),
('2023-10-12', 0, 0),
('2023-10-23', 0, 0),
('2023-10-24', 0, 0),
('2023-10-26', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobros`
--

CREATE TABLE `cobros` (
  `id_cobro` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `fecha_cobro` date NOT NULL,
  `monto` decimal(10,0) NOT NULL,
  `n_cuota` int(2) NOT NULL,
  `id_descuento` int(11) NOT NULL,
  `descuento` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cobros`
--

INSERT INTO `cobros` (`id_cobro`, `id_alumno`, `id_curso`, `fecha_cobro`, `monto`, `n_cuota`, `id_descuento`, `descuento`) VALUES
(35, 6, 1, '2023-09-28', '20000', 3, 2, 0.05),
(36, 6, 1, '2023-09-28', '20000', 4, 3, 0),
(37, 5, 1, '2023-10-11', '1530', 0, 1, 0.15),
(38, 5, 1, '2023-10-11', '4590', 0, 1, 0.15),
(49, 7, 1, '2023-10-12', '20000', 11, 1, 0.15),
(50, 7, 2, '2023-10-12', '30000', 21, 1, 0.15),
(51, 5, 51, '2023-10-12', '20000', 1, 3, 0),
(61, 5, 1, '2023-10-23', '10800', 6, 2, 0.05),
(62, 7, 2, '2023-10-23', '60000', 23, 1, 0.15),
(63, 7, 1, '2023-10-23', '20000', 6, 2, 0.05),
(64, 7, 2, '2023-10-23', '30000', 24, 2, 0.05),
(65, 1, 1, '2023-10-23', '2000', 1, 1, 0.15),
(66, 1, 51, '2023-10-23', '20000', 1, 1, 0.15),
(67, 7, 52, '2023-10-24', '4000', 2, 1, 0.15),
(68, 7, 52, '2023-10-26', '2000', 1, 1, 0.15),
(69, 7, 2, '2023-10-26', '0', 25, 2, 0.05),
(70, 7, 52, '2023-10-26', '2000', 3, 2, 0.05);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id_curso` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  `id_profesor` int(11) NOT NULL,
  `descripcion_curso` varchar(50) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `horario` time NOT NULL,
  `dia` varchar(10) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id_curso`, `id_materia`, `id_profesor`, `descripcion_curso`, `fecha_inicio`, `horario`, `dia`, `activo`) VALUES
(1, 3, 1, 'Herreria 4', '2023-11-07', '12:30:00', 'Lunes', 1),
(2, 4, 10, 'Ingles Nivel 2', '2023-09-21', '01:20:00', 'Miercoles', 1),
(51, 7, 2, 'Barista', '2004-07-15', '17:50:00', 'Martes', 1),
(52, 2, 8, 'Entrenamiento', '2023-10-20', '14:20:00', 'Jueves', 1),
(53, 8, 8, 'Conduccion', '2023-06-25', '14:25:00', 'Miercoles', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuentos`
--

CREATE TABLE `descuentos` (
  `id_descuento` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `porcentaje` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `descuentos`
--

INSERT INTO `descuentos` (`id_descuento`, `descripcion`, `porcentaje`) VALUES
(1, 'Efectivo', 0.15),
(2, 'Banco', 0.07),
(3, 'MercadoPago', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deudas`
--

CREATE TABLE `deudas` (
  `id_deuda` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `valor` int(100) NOT NULL,
  `valor2` int(100) NOT NULL,
  `cant_cuotas` int(11) NOT NULL,
  `cuotas_pagas` int(2) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `matricula` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `deudas`
--

INSERT INTO `deudas` (`id_deuda`, `id_alumno`, `id_curso`, `valor`, `valor2`, `cant_cuotas`, `cuotas_pagas`, `activo`, `matricula`) VALUES
(6, 6, 1, 20000, 10000, 6, 4, 1, 0),
(14, 7, 2, 30000, 0, 25, 25, 1, 0),
(15, 1, 1, 2000, 1000, 6, 1, 1, 0),
(17, 1, 51, 20000, 0, 6, 1, 1, 0),
(18, 7, 52, 2000, 1000, 6, 3, 1, 0),
(19, 7, 53, 60000, 70000, 6, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id_gasto` int(11) NOT NULL,
  `nombre_gasto` varchar(50) NOT NULL,
  `fecha_gasto` date NOT NULL,
  `monto_gasto` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id_gasto`, `nombre_gasto`, `fecha_gasto`, `monto_gasto`) VALUES
(9, 'Cafe', '2023-09-28', '1500');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `id_materia` int(100) NOT NULL,
  `nombre_materia` varchar(50) NOT NULL,
  `cuotas` int(100) NOT NULL,
  `valor` int(100) NOT NULL,
  `valor2` int(100) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id_materia`, `nombre_materia`, `cuotas`, `valor`, `valor2`, `descripcion`) VALUES
(2, 'matematicas', 6, 2000, 1000, 'fdgdfgdfhgfj'),
(3, 'lengua', 10, 13000, 10000, '23redwsdfsf'),
(4, 'ingles', 25, 30000, 0, 'dsfadasgfdasgh'),
(5, 'lengua', 54, 30000, 0, 'efgd345345'),
(6, 'contabilidad', 15, 25000, 0, 'materia contable con distintas ramas '),
(7, 'prueba', 6, 20000, 0, 'materia de prueba'),
(8, 'ingenieria', 6, 60000, 50000, 'fdgsdhfghjdfgjghj');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

CREATE TABLE `profesores` (
  `id_profesor` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `dni` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `num_direccion` int(11) NOT NULL,
  `cod_postal` int(4) NOT NULL,
  `telefono` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `observacion` varchar(50) NOT NULL,
  `foto` blob NOT NULL,
  `huella` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `profesores`
--

INSERT INTO `profesores` (`id_profesor`, `nombre`, `apellido`, `fecha_contratacion`, `dni`, `titulo`, `direccion`, `num_direccion`, `cod_postal`, `telefono`, `email`, `fecha_nacimiento`, `observacion`, `foto`, `huella`) VALUES
(1, 'Javier', 'Tamai', '2022-03-01', 28456789, 'Maestro mayor de obras', 'Lombardi', 4561, 0, 1125413835, 'tamaijavi@gmail.com', '1997-08-17', '0', '', ''),
(2, 'Franco ', 'Altamirano', '2023-03-01', 43993458, 'Tecnico en informatica', 'arana', 1234, 0, 1154457896, 'franconahuel@gmail.com', '2004-06-20', '0', '', ''),
(6, 'Maximo ', 'dominguez', '2023-06-09', 1132147092, 'profesorado en robotica', 'marsella', 2559, 1832, 1132147092, 'maximo@gmail.com', '2004-07-27', 'altos conocimientos c++ y c#', '', ''),
(8, 'oscar', 'ordoñez', '4223-03-12', 213423423, 'profesorado en Programacion', 'eva peron', 2134, 1234, 112324345, 'maximo@gmail.com', '4321-03-12', '324fedsfdsgfsgh', '', ''),
(9, 'octavio', 'crucero', '2243-04-23', 213545345, 'dsada', 'eva peron', 1234, 2134, 12321324, 'asfsbhed vgf@mf', '1998-03-21', 'dgfdgdfhgfh', '', ''),
(10, 'octavio', 'martinez', '2022-05-23', 234235, 'profesorado en Programacion', 'valparaiso', 2134, 1243, 11234543, 'df@vgf', '2001-03-12', '213sdfdsfdsg', '', ''),
(11, 'Maximo ', 'Medina', '4534-03-06', 564654, 'profesorado en Programacion', 'dsafgdasgdfas', 323454, 2134, 21324234, 'ju mjmb nj', '2354-04-23', 'qw34ewregdfg', '', ''),
(12, 'maximo ', 'dominguez', '2022-07-27', 45989523, 'profesorado en robotica', 'eva peron', 3245, 234234, 2147483647, 'dzfvgzaef@kkf', '2004-07-27', 'holahola', '', ''),
(13, 'valentin', 'gluszczuk', '2023-06-06', 5646213, 'profesorado en robotica', 'gpogh', 251, 4561, 1145258253, 'valentin@gmail.com', '2005-03-23', 'yuif', '', ''),
(14, 'oscar', 'benvidez', '2023-06-29', 2147483647, 'profesorado en matematicas', 'eva peron', 2132, 1324, 1132145374, 'oscarbenavidez@gmail.com', '1999-06-23', 'profesor con reconocimientos y recomendado', '', ''),
(15, 'prueba', 'prueba', '2023-07-27', 2147483647, 'pruebas', 'prueba ', 2559, 1832, 1132147092, 'maximolautaro04@gmail.com', '2000-05-25', 'pruebas', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `apellido` text NOT NULL,
  `email` text NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `contrasenia` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre`, `apellido`, `email`, `usuario`, `contrasenia`) VALUES
(1, '', '', '', 'Enrique', '456123'),
(3, 'admin', 'admin', 'admin@gmail.com', 'admin', 'admin'),
(4, 'maximo', 'dominguez', 'maximolautaro04@gmail.com', 'maxi', '1234');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id_alumno`);

--
-- Indices de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  ADD PRIMARY KEY (`id_asistencia`),
  ADD KEY `id_alumno` (`id_alumno`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `caja_diaria`
--
ALTER TABLE `caja_diaria`
  ADD PRIMARY KEY (`fecha_caja`);

--
-- Indices de la tabla `cobros`
--
ALTER TABLE `cobros`
  ADD PRIMARY KEY (`id_cobro`),
  ADD KEY `id_curso` (`id_curso`),
  ADD KEY `FOREING KEY` (`id_alumno`) USING BTREE,
  ADD KEY `id_descuento` (`id_descuento`),
  ADD KEY `fk_fecha_pago` (`fecha_cobro`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id_curso`),
  ADD KEY `id_profesor` (`id_profesor`),
  ADD KEY `id_materia` (`id_materia`);

--
-- Indices de la tabla `descuentos`
--
ALTER TABLE `descuentos`
  ADD PRIMARY KEY (`id_descuento`);

--
-- Indices de la tabla `deudas`
--
ALTER TABLE `deudas`
  ADD PRIMARY KEY (`id_deuda`),
  ADD KEY `id_alumno` (`id_alumno`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id_gasto`),
  ADD KEY `fk_fecha_gastos` (`fecha_gasto`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`id_materia`);

--
-- Indices de la tabla `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`id_profesor`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `id_alumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  MODIFY `id_asistencia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cobros`
--
ALTER TABLE `cobros`
  MODIFY `id_cobro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `descuentos`
--
ALTER TABLE `descuentos`
  MODIFY `id_descuento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `deudas`
--
ALTER TABLE `deudas`
  MODIFY `id_deuda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id_gasto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `materias`
--
ALTER TABLE `materias`
  MODIFY `id_materia` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `profesores`
--
ALTER TABLE `profesores`
  MODIFY `id_profesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cobros`
--
ALTER TABLE `cobros`
  ADD CONSTRAINT `cobros_ibfk_1` FOREIGN KEY (`id_alumno`) REFERENCES `alumnos` (`id_alumno`),
  ADD CONSTRAINT `cobros_ibfk_2` FOREIGN KEY (`id_curso`) REFERENCES `cursos` (`id_curso`),
  ADD CONSTRAINT `cobros_ibfk_3` FOREIGN KEY (`id_descuento`) REFERENCES `descuentos` (`id_descuento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fecha_pago` FOREIGN KEY (`fecha_cobro`) REFERENCES `caja_diaria` (`fecha_caja`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `fk_id_materia` FOREIGN KEY (`id_materia`) REFERENCES `materias` (`id_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_id_profesor` FOREIGN KEY (`id_profesor`) REFERENCES `profesores` (`id_profesor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `deudas`
--
ALTER TABLE `deudas`
  ADD CONSTRAINT `fk_id_alumnos` FOREIGN KEY (`id_alumno`) REFERENCES `alumnos` (`id_alumno`),
  ADD CONSTRAINT `fk_id_cursos` FOREIGN KEY (`id_curso`) REFERENCES `cursos` (`id_curso`);

--
-- Filtros para la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD CONSTRAINT `fk_fecha_gastos` FOREIGN KEY (`fecha_gasto`) REFERENCES `caja_diaria` (`fecha_caja`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
