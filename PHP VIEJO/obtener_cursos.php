<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Consulta para obtener los cursos
$sql = "SELECT id, nombre FROM cursos";
$result = $conn->query($sql);

// Crear un array para almacenar los resultados
$cursos = array();

// Obtener los datos de los cursos y agregarlos al array
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $curso = array(
            'id' => $row['id'],
            'nombre' => $row['nombre']
        );
        array_push($cursos, $curso);
    }
}

// Cerrar la conexión a la base de datos
$conn->close();

// Enviar los datos como respuesta en formato JSON
header('Content-Type: application/json');
echo json_encode($cursos);
mysqli_close($conn);
?>
