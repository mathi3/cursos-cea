<?php
$host = "localhost"; // nombre del servidor de base de datos
$usuario = "root"; // nombre de usuario para acceder a la base de datos
$contrasena = ""; // contrasena para acceder a la base de datos
$dbname= "cea_base_de_datos"; // nombre de la base de datos

// Conexion a la base de datos
$conn = mysqli_connect($host, $usuario, $contrasena, $dbname);

// Verificar si la conexion es exitosa
if (!$conn) {
    die("Conexion fallida: " . mysqli_connect_error());
}

echo "Conexion exitosa";
?>