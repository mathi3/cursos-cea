<?php
// Conexión a la base de datos (reemplaza con tus propios datos de conexión)
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

// Obtener el ID de materia enviado desde el cliente mediante AJAX
if (isset($_GET['id_materia'])) {
    $idMateria = $_GET['id_materia'];

    // Crear la conexión
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Verificar la conexión
    if ($conn->connect_error) {
        die("Error de conexión: " . $conn->connect_error);
    }

    // Consulta SQL para obtener los datos de la materia
    $sql = "SELECT cuotas, valor FROM alta_materia WHERE id_materia = $idMateria";

    // Ejecutar la consulta y obtener los resultados
    $result = $conn->query($sql);

    // Verificar si se obtuvieron resultados y enviarlos como respuesta JSON
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $data = array(
            'cuotas' => $row['cuotas'],
            'valor' => $row['valor']
        );
        echo json_encode($data);
    } else {
        echo "No se encontraron datos de la materia.";
    }

    // Cerrar la conexión a la base de datos
    $conn->close();
} else {
    echo "ID de materia no especificado.";
}
?>

