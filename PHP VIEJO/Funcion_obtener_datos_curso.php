<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Obtener el id del curso desde la solicitud GET
$cursoId = $_GET['id_curso'];

// Obtener el valor y cuotas del curso desde la tabla alta_materia
$sql = "SELECT am.valor, am.cuotas FROM materias am INNER JOIN cursos c ON am.id_materia = c.id_materia WHERE c.id_curso = '$cursoId'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $data = array(
        "valor" => $row["valor"],
        "cuotas" => $row["cuotas"]
    );
    echo json_encode($data);
} else {
    echo "No se encontraron datos del curso.";
}

$conn->close();
?>

