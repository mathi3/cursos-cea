<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cea_base_de_datos";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
}

// Consulta a la base de datos para obtener los cursos con los datos de la materia y el profesor
$sql = "SELECT am.nombre_materia AS materia, CONCAT(ap.nombre, ' ', ap.apellido) AS profesor, c.nombre, c.fecha_inicio, c.horario_ingreso, c.observacion FROM cursos c
INNER JOIN alta_materia am ON c.id_materia = am.id_materia 
INNER JOIN alta_profesores ap ON c.id_profesor = ap.id_profesor";
$result = $conn->query($sql);


// Obtener los datos de los cursos en formato JSON
$cursos = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $cursos[] = $row;
    }
}

$conn->close();

// Enviar los datos de los cursos en formato JSON
header('Content-Type: application/json');
echo json_encode($cursos);
?>
