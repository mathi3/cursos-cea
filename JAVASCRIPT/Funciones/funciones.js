export function obtenerFechaActual() {
  // Obtenemos la fecha actual
  const fechaActual = new Date();

  // Obtenemos el día, mes y año de la fecha actual
  const dia = fechaActual.getDate();
  const mes = fechaActual.getMonth() + 1; // Sumamos 1 al mes, ya que en JavaScript los meses van de 0 a 11
  const anio = fechaActual.getFullYear();

  // Formateamos la fecha en formato "YYYY_MM_DD"
  const fechaFormateada = `${anio.toString()}-${mes.toString().padStart(2, '0')}-${dia.toString().padStart(2, '0')}`;

  return fechaFormateada;
}

//Funcion para traer los valores de los descuentos desde la tabla descuentos
export function cargarDescuentos(callback) {
  $.ajax({
      url: "../../PHP/cargar_descuentos.php",
      type: "POST",
      success: function (data) {
          callback(data); // Llamar a la función de devolución de llamada con los datos
      },
      error: function (xhr, status, error) {
          alert("Ocurrió un error al cargar los descuentos");
      }
  });
}