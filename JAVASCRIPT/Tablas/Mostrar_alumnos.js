$(document).ready(function() {
    $.ajax({
      url: '../../PHP/Tablas/Mostrar_alumnos.php',
      dataType: 'json',
      success: function(data) {
        var tablaBody = $('#tabla-alumnos-body');
        
        if (data.length > 0) {
          // Almacenar los datos de los alumnos en una variable
          var alumnos = data;
  
          // Mostrar todos los alumnos al cargar la página
          mostrarAlumnos(alumnos);
  
          // Escuchar el evento de entrada en el campo de búsqueda
          $('#input-busqueda').on('input', function() {
            var filtro = $(this).val();
            var alumnosFiltrados = filtrarAlumnos(alumnos, filtro);
            mostrarAlumnos(alumnosFiltrados);
          });
        } else {
          var fila = $('<tr>');
          fila.append($('<td colspan="10">').text('No se encontraron alumnos'));
  
          tablaBody.append(fila);
        }
      },
      error: function() {
        console.log('Error al obtener los datos de los alumnos');
      }
    });
  });
  
  // Función para mostrar los alumnos en la tabla
  function mostrarAlumnos(alumnos) {
    var tablaBody = $('#tabla-alumnos-body');
    tablaBody.empty();
  
    if (alumnos.length > 0) {
      $.each(alumnos, function(index, alumno) {
        var fila = $('<tr>');
        fila.append($('<td>').text(alumno.nombre));
        fila.append($('<td>').text(alumno.apellido));
        fila.append($('<td>').text(alumno.dni));
        fila.append($('<td>').text(alumno.fecha_inscripcion));
        fila.append($('<td>').text(alumno.empresa));
        fila.append($('<td>').text(alumno.fecha_nacimiento));
        fila.append($('<td>').text(alumno.direccion));
        fila.append($('<td>').text(alumno.codigo_postal));
        fila.append($('<td>').text(alumno.telefono));
        fila.append($('<td>').text(alumno.email));
  
        crearBtnEdit(alumno, fila)

          // Crear un botón
        var botonEliminar = document.createElement("button");
        botonEliminar.innerHTML = "X";
                        
        //Le agrego al boton una id correspondiente a su id en la bd
        botonEliminar.setAttribute("id", alumno.id_alumno);
        //Le agrego al boton la clase back-button, para que tenga un diseño
        botonEliminar.setAttribute("class", "back-button");
        
        botonEliminar.addEventListener("click", function() {
          var respuesta = confirm("¿Desea eliminar esta alumno? Tenga en cuenta que su deuda no se borrara.");
          if (respuesta){
            // Crear un objeto FormData con el id de la materia
            var formData = new FormData();
            formData.append('id', alumno.id_alumno);
            
            // Realiza una solicitud al servidor para eliminar la materia
            fetch('../../PHP/eliminar_alumno.php', {
            method: 'POST',
            body: formData // Pasar el objeto FormData como el cuerpo de la solicitud
          })
            .then(function(response) {
              if (response.ok) {
                // Si la eliminación fue exitosa, elimina la fila del botón y recarga la pagina para mostrar los cambios
                fila.remove();
              } else {
                // Maneja errores o muestra un mensaje de error al usuario
                console.error('Error al eliminar el alumno');
              }
            })
            .catch(function(error) {
              console.error('Error de red: ' + error);
                });
              }
              });
        //Agregar el boton a la fila
        fila.append($('<td>').append(botonEliminar));

        tablaBody.append(fila);
      });
    } else {
      var fila = $('<tr>');

      fila.append($('<td colspan="10">').text('No se encontraron alumnos'));
  
      tablaBody.append(fila);
    }
  }

  /*CREAR FUNCION ONCLICK PARA QUE CUANDO SE TOCA LA FILA DEL ALUMNO ELEGIDO SE DESPLIGUE LOS DATOS DE LAS MATERIAS ASIGNADAS PARA PODER VER Y MODIFICAR LA ASIGNACION DE
  CURSO DE CADA UNO (TAMBIEN VER COMO MODIFICAR EN TIEMPO REAL)*/
  
  // Función para filtrar los alumnos según el criterio de búsqueda
  function filtrarAlumnos(alumnos, filtro) {
    return alumnos.filter(function(alumno) {
      var nombreCompleto = alumno.nombre.toLowerCase() + ' ' + alumno.apellido.toLowerCase();
      return nombreCompleto.startsWith(filtro.toLowerCase());
    });
  }
  
  function crearBtnEdit(alumno, fila){
    // Crear un botón
    var boton = document.createElement("button");
    boton.innerHTML = "Editar";
    
    //Le agrego al boton una id correspondiente a su id en la bd
    boton.setAttribute("id", alumno.id_alumno);
    //Le agrego al boton la clase back-button, para que tenga un diseño
    boton.setAttribute("class", "edit-button");
    
    //Le agrego un EventListener, detecta cuando se hace click en un boton y lo envia al formulario de edicion de materia.
    boton.addEventListener("click", function() {
      window.location.href = "../Formularios/Formulario_modif_alumno.html?id=" + this.id;
    });
  
    //Agregar el boton a la fila
    fila.append($('<td>').append(boton));
}