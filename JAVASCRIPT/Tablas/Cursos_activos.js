$(document).ready(function() {
    $.ajax({
        url: '../PHP/obtener_cursos_activos.php',
        dataType: 'json',
        success: function(data) {
            var tablaBody = $('#cursos-activos-body');
            
            if (data.length > 0) {
                // Almacenar los datos de los cursos en una variable
                var cursos = data;
    
                // Mostrar todos los cursos al cargar la página
                mostrarCursosActivos(cursos);
            } else {
                var fila = $('<tr>');
                fila.append($('<td colspan="10">').text('No se encontraron cursos'));
    
                tablaBody.append(fila);
            }
        },
        error: function() {
            console.log('Error al obtener los datos de los cursos');
        }
    });
});

// Función para mostrar los cursos activos
function mostrarCursosActivos(cursos) {
    var tablaBody = $('#cursos-activos-body');
    tablaBody.empty();

    if (cursos.length > 0) {
        $.each(cursos, function(index, curso) {
            var fila = $('<tr>');
            fila.append($('<td>').text(curso.materia));
            fila.append($('<td>').text(curso.profesor));
            fila.append($('<td>').text(curso.nombre));
            fila.append($('<td>').text(curso.fecha_inicio));
            fila.append($('<td>').text((curso.horario_ingreso).split('.')[0]));
            fila.append($('<td>').text(curso.observacion));

            tablaBody.append(fila);
        });
    } else {
        var fila = $('<tr>');
        fila.append($('<td colspan="10">').text('No se encontraron cursos'));

        tablaBody.append(fila);
    }
}