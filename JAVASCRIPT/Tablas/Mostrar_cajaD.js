// Definir montoIni aquí para que esté disponible en todo el script
var montoIni = 0;

//Funcion para mostrar el monto actual
function actualizar_monto() {
  //Mostrar el total del monto actual de la caja diaria -- TIENE QUE ESTAR ACA PARA QUE SE ACTUALIZE CORRECTAMENTE
  var totalP = sumar_columnas("monto_p");
  var totalG = sumar_columnas("monto_g");
  document.getElementById("total").textContent = (montoIni + totalP) - totalG;
  document.getElementById("monto_ini").textContent = montoIni;

  var celdasSum = document.getElementsByClassName("monto_p");
  var celdasCat = document.getElementsByClassName("tipoP");
  var totalEf = 0;
  for (var i = 0; i < celdasSum.length; i++) {
    if(celdasCat[i].textContent=="Efectivo"){
      var contenido = celdasSum[i].textContent;
      var valorSinSigno = Number(contenido.replace('$', '')); // Eliminar el signo "$" y convertir a número
      totalEf += valorSinSigno;
    }
  }
  document.getElementById("monto_ef").textContent = totalEf+montoIni;
  document.getElementById("monto_ret").textContent = totalG;
}

$(document).ready(function () {
  //Cargar los pagos realizados por los alumnos -- ¡¡¡ CAMBIAR NOMBRE a COBROS de ALUMNOS !!! --> Los pagos son las cosas que salen del monto de la caja diaria
  $.ajax({
    url: '../PHP/Tablas/Mostrar_cobros.php',
    dataType: 'json',
    success: function (data) {
      var tablaBody = $('#tabla-cobros-body');

      if (data.length > 0) {
        // Almacenar los datos de los pagos en una variable
        var pagos = data;

        // Mostrar todos los pagos al cargar la página
        tablaBody.empty();
        $.each(pagos, function (index, pago) {
          var fila = $('<tr>');
          fila.append($('<td>').text(pago.id_cobro));
          fila.append($('<td>').text(pago.nombre));
          fila.append($('<td>').text(pago.apellido));
          fila.append($('<td>').text(pago.dni));
          fila.append($('<td>').text(pago.descripcion_curso));
          fila.append($('<td>').text(pago.fecha_cobro));
          fila.append($('<td>').text(pago.n_cuota));
          fila.append($('<td class="monto_p">').text('$' + pago.monto));
          fila.append($('<td class="tipoP">').text(pago.tipo_pago));
          // Agregar el botón "Editar" llamando a la función crearBtnEdit
          crearBtnImp(pago, fila);
 
          tablaBody.append(fila);
        });

        // Escuchar el evento de entrada en el campo de búsqueda
        $('#input-busqueda').on('input', function () {
          var filtro = $(this).val();
          var pagosFiltrados = filtrarPagos(pagos, filtro);
          mostrarPagos(pagosFiltrados);
        });
      } else {
        var fila = $('<tr>');
        fila.append($('<td colspan="9">').text('No se encontraron cobros'));

        tablaBody.append(fila);
      }
      actualizar_monto();
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error en la solicitud Ajax:');
      console.log('Estado: ' + textStatus);
      console.log('Error: ' + errorThrown);

      // Si el servidor devuelve un mensaje de error, también puedes acceder a él así:
      if (jqXHR.responseJSON && jqXHR.responseJSON.error) {
          console.log('Mensaje de error del servidor: ' + jqXHR.responseJSON.error);
      }
  }
  });
    $.ajax({
      url: '../PHP/Tablas/Mostrar_gastos.php',
      dataType: 'json',
      success: function(data) {
        var tablaBody = $('#tabla-gastos-body');
  
        if (data.length > 0) {
          // Almacenar los datos de los gastos en una variable
          var gastos = data;
  
          // Mostrar todos los gastos al cargar la página
          mostrarGastos(gastos);
  
          // Escuchar el evento de entrada en el campo de búsqueda
          $('#input-busqueda').on('input', function() {
            var filtro = $(this).val();
            var gastosFiltrados = filtrarPagos(gastos, filtro);
            mostrarGastos(gastosFiltrados);
          });
        } else {
          var fila = $('<tr>');
          fila.append($('<td colspan="9">').text('No se encontraron pagos'));
  
          tablaBody.append(fila);
        }
        actualizar_monto();
      },
      error: function() {
        console.log('Error al obtener los datos de los cobros');
      }
    });
});

// Función para sumar los valores de las columnas
function sumar_columnas(col) {
  var celdas = document.getElementsByClassName(col);

  var total = 0;
  for (var i = 0; i < celdas.length; i++) {
    var contenido = celdas[i].textContent;
    var valorSinSigno = Number(contenido.replace('$', '')); // Eliminar el signo "$" y convertir a número
    total += valorSinSigno;
  }
  return total;
}


function mostrarPagos(pagos) {
  var tablaBody = $('#tabla-cobros-body');
  tablaBody.empty();
  $.each(pagos, function (index, pago) {
    var fila = $('<tr>');
    let cuota = pago.cuotas_pagas + " de " + pago.cant_cuotas;
    fila.append($('<td>').text(pago.id_cobro));
    fila.append($('<td>').text(pago.nombre));
    fila.append($('<td>').text(pago.apellido));
    fila.append($('<td>').text(pago.dni));
    fila.append($('<td>').text(pago.nombre_curso));
    fila.append($('<td>').text(pago.fecha_cobro));
    fila.append($('<td>').text(cuota));
    fila.append($('<td class="monto_p">').text('$' + pago.monto));
    fila.append($('<td>').text(pago.tipo_pago));


    tablaBody.append(fila);
  });
}

//Funcion para mostrar los cobros cargados en la BD en la tabla
function mostrarGastos(gastos) {
  var tablaBody = $('#tabla-gastos-body');
  tablaBody.empty();

  if (gastos.length > 0) {
    $.each(gastos, function (index, gasto) {
      var fila = $('<tr>');
      fila.append($('<td>').text(gasto.id_gasto));
      fila.append($('<td>').text(gasto.nombre_gasto));
      fila.append($('<td>').text(gasto.categoria));
      fila.append($('<td>').text(gasto.fecha_gasto));
      fila.append($('<td class="monto_g">').text('$' + gasto.monto_gasto));

      tablaBody.append(fila);
    });
  } else {
    var fila = $('<tr>');
    fila.append($('<td colspan="9">').text('No se encontraron pagos'));

    tablaBody.append(fila);
  }
}

// Función para filtrar los pagos según el criterio de búsqueda
function filtrarPagos(pagos, filtro) {
  return pagos.filter(function (pago) {
    var nombreCompleto = pago.nombre.toLowerCase() + ' ' + pago.apellido.toLowerCase();
    return nombreCompleto.startsWith(filtro.toLowerCase());
  });
}

//Funcion para guardar la pagina como un PDF
function imprimir_pdf() {
  var elemento = document.getElementById("imprimir");//Guardo el elemento a imprimir
  //Configuraciones de la impresión
  var opt = {
    margin: 1, //Valores de los margenes
    filename: 'CajaDiaria ' + ((new Date()).toLocaleDateString("es-AR")) + '.pdf', //Nombre del archivo al descargar, toma la fecha actual para mejor identifiacion.
    html2canvas: { scale: 1 }, //Escala que se utiliza a la hora de la impresion
    jsPDF: { unit: 'cm', format: 'a4', orientation: 'landscape' } //Configuracion de impresion. Se especifica la unidad de medidas (cm en este caso), formato de hoja por defecto en a4, orientacion horizontal.
  };

  html2pdf().set(opt).from(elemento).save(); //Guardo el pdf con lo que contiene la variable elemento, al que se le aplican las configuraciones guardadas en opt.
}

document.addEventListener("DOMContentLoaded", function () {

  $.ajax({
    url: '../PHP/Tablas/cargar_cajaD.php',
    dataType: 'json',
    success: function (data) {
      if (data.apertura !== undefined) { //Si existen datos cargados previamente en el dia de hoy, se actualiza el monto inicial, si no, queda en 0 (para evitar errores)
        montoIni = Number(data.apertura);
      }
      actualizar_monto();

    },
    error: function () {
      console.log('No se encontrar datos cargados');
    }
  });

  // Agregar evento al botón "Cargar monto inicial"
  var cargarMontoBtn = document.getElementById("monto_inicial");
  cargarMontoBtn.addEventListener("click", function () {
    // Mostrar el overlay
    var overlay = document.getElementById("overlay");
    overlay.style.display = "flex";
  });

  //Agregar evento al botón "Cierre de caja" --------TERMINAR -------
  /*var cierreBtn = document.getElementById("cierre");
  cierreBtn.addEventListener("click", function () {
    //Guardo la parte del monto total
    var estadoTot= document.getElementById("total");

    // Verifica si el contenido del monto actual está vacío
      if (estadoTot.textContent.trim() === '') {
        // Ejecuta cierta parte del código si el contenido está vacío
        alert('No se detectaron datos cargados.');
      } else {
        // Ejecuta otra parte del código si el contenido no está vacío
        $.ajax({
          url: '../PHP/Tablas/cierre_cajaD.php',
          dataType: 'json',
          method: 'POST', 
          data: {
              montoIni: montoIni,
              montoTot: Number(estadoTot.textContent)
          },
          success: function() {
            // Actualizar el monto en la interfaz
            actualizar_monto();
          },
          error: function() {
              console.log('Error al cerrar la caja.');
          }
      });
      }

  });*/

  // Agregar evento al botón "Aceptar" del formulario
  var submitMonto = document.getElementById("submitMonto");
  var montoInput = document.getElementById("montoInput");
  submitMonto.addEventListener("click", function () {
    var montoIngresado = parseFloat(montoInput.value);
    if (!isNaN(montoIngresado)) {
      // Ocultar el overlay
      var overlay = document.getElementById("overlay");
      overlay.style.display = "none";
      // Actualizar el valor de montoIni
      montoIni = montoIngresado;

      $.ajax({
        url: '../PHP/Tablas/actualizar_cajaD.php',
        dataType: 'json',
        method: 'POST',
        data: {
          montoIni: montoIni
        },
        success: function () {
          // Actualizar el monto en la interfaz
          actualizar_monto();
        },
        error: function () {
          console.log('Error al cargar la apertura de caja.');
        }
      });

    } else {
      // El usuario ingresó un valor no válido
      alert("Por favor, ingrese un monto válido.");
    }
  });

});

function generarPDF(pago, fila) {
  // Crear un elemento div para el contenido del PDF
  var contenidoPDF = document.createElement("div");

  // Agregar los datos de la fila al contenido del PDF
  contenidoPDF.innerHTML = `
    <h2 style="margin-left: 3%; margin-top: 5%;">Comprobante de Pago</h2>
    <br>
    <p>ID de Pago: ${pago.id_cobro}</p>
    <p>Nombre: ${pago.nombre}</p>
    <p>Apellido: ${pago.apellido}</p>
    <p>DNI: ${pago.dni}</p>
    <p>Curso: ${pago.descripcion_curso}</p>
    <p>Fecha de Pago: ${pago.fecha_cobro}</p>
    <p>N° de Cuota: ${pago.n_cuota}</p>
    <p>Monto: $${pago.monto}</p>
    <p>Tipo de Pago: ${pago.tipo_pago}</p>
  `;

  var opt = {
    margin: 1, //Valores de los margenes
    filename: 'Comprobante_pago_ ' +pago.id_cobro+'.pdf', //Nombre del archivo al descargar, toma la fecha actual para mejor identifiacion.
    html2canvas: { scale: 1 }, //Escala que se utiliza a la hora de la impresion
    jsPDF: { unit: 'cm', format: 'a4', orientation: 'portrait' } //Configuracion de impresion. Se especifica la unidad de medidas (cm en este caso), formato de hoja por defecto en a4, orientacion horizontal.
  };

  html2pdf().set(opt).from(contenidoPDF).save(); //Guardo el pdf con lo que contiene la variable elemento, al que se le aplican las configuraciones guardadas en opt.

}

function crearBtnImp(pago, fila) {
  // Crear un botón
  var boton = document.createElement("button");
  boton.innerHTML = "Imprimir";

  // Le agrego al botón una clase para darle estilo
  boton.setAttribute("class", "back-button");

  // Le agrego un EventListener para generar el PDF al hacer clic en el botón
  boton.addEventListener("click", function () {
    generarPDF(pago, fila);
  });

  // Agregar el botón a la fila
  fila.append($('<td>').append(boton));
}
