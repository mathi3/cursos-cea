$(document).ready(function() {
    $.ajax({
      url: '../../PHP/Tablas/Mostrar_profesores.php',
      dataType: 'json',
      success: function(data) {
        var tablaBody = $('#tabla-profesores-body');
  
        if (data.length > 0) {
          // Almacenar los datos de los profesores en una variable
          var profesores = data;
  
          // Mostrar todos los profesores al cargar la página
          mostrarProfesores(profesores);
  
          // Escuchar el evento de entrada en el campo de búsqueda
          $('#input-busqueda').on('input', function() {
            var filtro = $(this).val();
            var profesoresFiltrados = filtrarProfesores(profesores, filtro);
            mostrarProfesores(profesoresFiltrados);
          });
        } else {
          var fila = $('<tr>');
          fila.append($('<td colspan="11">').text('No se encontraron profesores'));
  
          tablaBody.append(fila);
        }
      },
      error: function() {
        console.log('Error al obtener los datos de los profesores');
      }
    });
  });

  // Función para mostrar los profesores en la tabla
  function mostrarProfesores(profesores) {
    var tablaBody = $('#tabla-profesores-body');
    tablaBody.empty();
  
    if (profesores.length > 0) {
      $.each(profesores, function(index, profesor) {
        var fila = $('<tr>');
        fila.append($('<td>').text(profesor.nombre));
        fila.append($('<td>').text(profesor.apellido));
        fila.append($('<td>').text(profesor.fecha_contratacion));
        fila.append($('<td>').text(profesor.dni));
        fila.append($('<td>').text(profesor.titulo));
        fila.append($('<td>').text(profesor.direccion));
        fila.append($('<td>').text(profesor.cod_postal));
        fila.append($('<td>').text(profesor.telefono));
        fila.append($('<td>').text(profesor.email));
        fila.append($('<td>').text(profesor.fecha_nacimiento));
        fila.append($('<td>').text(profesor.observacion));
  
        crearBtnEdit(profesor, fila);

        // Crear un botón
      var botonEliminar = document.createElement("button");
      botonEliminar.innerHTML = "X";
                      
      //Le agrego al boton una id correspondiente a su id en la bd
      botonEliminar.setAttribute("id", profesor.id_profesor);
      //Le agrego al boton la clase back-button, para que tenga un diseño
      botonEliminar.setAttribute("class", "back-button");
      
      botonEliminar.addEventListener("click", function() {
        var respuesta = confirm("¿Desea eliminar este profesor?");
        if (respuesta){
          // Crear un objeto FormData con el id de la materia
          var formData = new FormData();
          formData.append('id', profesor.id_profesor);
          
          // Realiza una solicitud al servidor para eliminar la materia
          fetch('../../PHP/eliminar_profesor.php', {
          method: 'POST',
          body: formData // Pasar el objeto FormData como el cuerpo de la solicitud
        })
          .then(function(response) {
            if (response.ok) {
              // Si la eliminación fue exitosa, elimina la fila del botón para mostrar los cambios
              fila.remove();
            } else {
              // Maneja errores o muestra un mensaje de error al usuario
              console.error('Error al eliminar el profesor');
            }
          })
          .catch(function(error) {
            console.error('Error de red: ' + error);
              });
            }
            });
      //Agregar el boton a la fila
      fila.append($('<td>').append(botonEliminar));

      tablaBody.append(fila);
      });
    } else {
      var fila = $('<tr>');
      fila.append($('<td colspan="11">').text('No se encontraron profesores'));
  
      tablaBody.append(fila);
    }
  }
  
  // Función para filtrar los profesores según el criterio de búsqueda
  function filtrarProfesores(profesores, filtro) {
    return profesores.filter(function(profesor) {
      var nombreCompleto = profesor.nombre.toLowerCase() + ' ' + profesor.apellido.toLowerCase();
      return nombreCompleto.startsWith(filtro.toLowerCase());
    });
  }
  
  function crearBtnEdit(profesor, fila){
    // Crear un botón
    var boton = document.createElement("button");
    boton.innerHTML = "Editar";
    
    //Le agrego al boton una id correspondiente a su id en la bd
    boton.setAttribute("id", profesor.id_profesor);
    //Le agrego al boton la clase back-button, para que tenga un diseño
    boton.setAttribute("class", "edit-button");
    
    //Le agrego un EventListener, detecta cuando se hace click en un boton y lo envia al formulario de edicion de materia.
    boton.addEventListener("click", function() {
      window.location.href = "../Formularios/Formulario_modif_profesor.html?id=" + this.id;
    });
  
    //Agregar el boton a la fila
    fila.append($('<td>').append(boton));
}