$(document).ready(function() {
  $.ajax({
    url: '../../PHP/Tablas/Mostrar_materias.php',
    dataType: 'json',
    success: function(data) {
      var tablaBody = $('#tabla-materias-body');

      if (data.length > 0) {
        // Almacenar los datos de las materias en una variable
        var materias = data;

        // Mostrar todas las materias al cargar la página
        mostrarMaterias(materias);

        // Escuchar el evento de entrada en el campo de búsqueda
        $('#input-busqueda').on('input', function() {
          var filtro = $(this).val();
          var materiasFiltradas = filtrarMaterias(materias, filtro);
          mostrarMaterias(materiasFiltradas);
        });
      } else {
        var fila = $('<tr>');
        fila.append($('<td colspan="4">').text('No se encontraron materias'));

        tablaBody.append(fila);
      }
    },
    error: function() {
      console.log('Error al obtener los datos de las materias');
    }
  });
});

// Función para mostrar las materias en la tabla
function mostrarMaterias(materias) {
  var tablaBody = $('#tabla-materias-body');
  tablaBody.empty();

  if (materias.length > 0) {
    $.each(materias, function(index, materia) {
      var fila = $('<tr>');
      fila.append($('<td>').text(materia.nombre_materia));
      fila.append($('<td>').text(materia.descripcion));
      fila.append($('<td>').text(materia.valor));
      fila.append($('<td>').text(materia.valor2));
      fila.append($('<td>').text(materia.cuotas));
      
      crearBtnEdit(materia, fila);

      // Crear un botón
      var botonEliminar = document.createElement("button");
      botonEliminar.innerHTML = "X";
                      
      //Le agrego al boton una id correspondiente a su id en la bd
      botonEliminar.setAttribute("id", materia.id_materia);
      //Le agrego al boton la clase back-button, para que tenga un diseño
      botonEliminar.setAttribute("class", "back-button");
      
      botonEliminar.addEventListener("click", function() {
        var respuesta = confirm("¿Desea eliminar esta materia?");
        if (respuesta){
          // Crear un objeto FormData con el id de la materia
          var formData = new FormData();
          formData.append('id', materia.id_materia);
          
          // Realiza una solicitud al servidor para eliminar la materia
          fetch('../../PHP/eliminar_materia.php', {
          method: 'POST',
          body: formData // Pasar el objeto FormData como el cuerpo de la solicitud
        })
          .then(function(response) {
            if (response.ok) {
              // Si la eliminación fue exitosa, elimina la fila del botón y recarga la pagina para mostrar los cambios
              fila.remove();
            } else {
              // Maneja errores o muestra un mensaje de error al usuario
              console.error('Error al eliminar la materia');
            }
          })
          .catch(function(error) {
            console.error('Error de red: ' + error);
              });
            }
            });
      //Agregar el boton a la fila
      fila.append($('<td>').append(botonEliminar));

      tablaBody.append(fila);
    });
  } else {
    var fila = $('<tr>');
    fila.append($('<td colspan="4">').text('No se encontraron materias'));

    tablaBody.append(fila);
  }
}

// Función para filtrar las materias según el criterio de búsqueda
function filtrarMaterias(materias, filtro) {
  return materias.filter(function(materia) {
    return materia.nombre.toLowerCase().startsWith(filtro.toLowerCase());
  });
}

// Función para filtrar las materias según el criterio de búsqueda
function filtrarMaterias(materias, filtro) {
  return materias.filter(function(materia) {
    var nombreMateria = materia.nombre_materia.toLowerCase();
    return nombreMateria.startsWith(filtro.toLowerCase());
  });
}


function goBack() {
  window.location.href = "../Cursos.html";
}

function crearBtnEdit(materia, fila){
      // Crear un botón
      var boton = document.createElement("button");
      boton.innerHTML = "Editar";
      
      //Le agrego al boton una id correspondiente a su id en la bd
      boton.setAttribute("id", materia.id_materia);
      //Le agrego al boton la clase back-button, para que tenga un diseño
      boton.setAttribute("class", "edit-button");
      
      //Le agrego un EventListener, detecta cuando se hace click en un boton y lo envia al formulario de edicion de materia.
      boton.addEventListener("click", function() {
        window.location.href = "../Formularios/Formulario_modif_materia.html?id=" + this.id;
      });
    
      //Agregar el boton a la fila
      fila.append($('<td>').append(boton));
}