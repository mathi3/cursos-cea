import { obtenerFechaActual } from "../Funciones/funciones.js"

$(document).ready(function() {
    $.ajax({
        url: '../PHP/Tablas/Mostrar_cursos.php',
        dataType: 'json',
        success: function(data) {
            var tablaBody = $('#tabla-cursos-body');

            if (data.length > 0) {
                // Almacenar los datos de los cursos en una variable
                var cursos = data;

                // Mostrar todos los cursos al cargar la página
                mostrarCursos(cursos);

                // Escuchar el evento de entrada en el campo de búsqueda
                $('#input-busqueda').on('input', function() {
                    var filtro = $(this).val();
                    var cursosFiltrados = filtrarCursos(cursos, filtro);
                    mostrarCursos(cursosFiltrados);
                });
            } else {
                var fila = $('<tr>');
                fila.append($('<td colspan="10">').text('No se encontraron cursos'));

                tablaBody.append(fila);
            }
        },
        error: function() {
            console.log('Error al obtener los datos de los cursos');
        }
    });
});


// Función para mostrar los cursos en la tabla
function mostrarCursos(cursos) {
    var tablaBody = $('#tabla-cursos-body');
    tablaBody.empty();

    if (cursos.length > 0) {
        console.log(cursos);
        $.each(cursos, function(index, curso) {
            var fila = $('<tr>');
            fila.append($('<td>').text(curso.id_curso));
            fila.append($('<td>').text(curso.descripcion_curso));
            fila.append($('<td>').text(curso.profesor));
            fila.append($('<td>').text(curso.materia));
            fila.append($('<td>').text(curso.dia));
            fila.append($('<td>').text(curso.horario));
            fila.append($('<td>').text(curso.inscriptos));
            
            // Agrega un checkbox según el valor de curso.activo
            var checkbox = $('<input type="checkbox">');
            if (curso.activo === '1' || curso.activo === 1 || curso.activo.toLowerCase() === 'activo') {
                checkbox.prop('checked', true);
            }

            fila.append($('<td>').append(checkbox));

            // Agrega un manejador de eventos 'change' al checkbox
            checkbox.on('change', function() {
                var nuevoValor = checkbox.prop('checked') ? 1 : 0;

                if(nuevoValor==1){
                    $('.form-container').css('display', 'flex');

                    // Bloquear el fondo detrás del formulario
                    $('body').css('overflow', 'hidden');
                    document.getElementById('bloqueador').style.display = 'block';

                    document.getElementById('fecha_inicio').value = obtenerFechaActual();

                    $('#aceptarFecha').on('click', function (event) {
                        // Evitar que el formulario se recargue
                        event.preventDefault();
                        var fechaIni=document.getElementById('fecha_inicio').value
                        // Realiza una solicitud AJAX para actualizar la fecha de inicio del curso.
                        $.ajax({
                            url: '../PHP/Tablas/actualizar_estado_curso.php',
                            method: 'POST',
                            data: {
                                cursoId: curso.id_curso, // Envía el ID del curso
                                nuevoValor: nuevoValor, // Envía el nuevo valor (1 o 0)
                                fechaIni: fechaIni, //Envia la fecha del nuevo inicio
                                fun: 0 //Valor de control
                            },
                            success: function(response) {
                                // Actualización exitosa
                                console.log("Exito");
                                $('.form-container').css('display', 'none');
                                document.getElementById('bloqueador').style.display = 'none';
                            },
                            error: function(xhr, status, error) {
                                // Maneja errores aquí
                                alert('Error al actualizar el valor en la base de datos: ' + error);
                            }
                        });
                    });
                    
                    $('#cancelarFecha').on('click', function (event) {
                        console.log("CANCELAR");
                        // Evitar que el formulario se recargue
                        event.preventDefault();
                        checkbox.prop('checked', false);
                        $('.form-container').css('display', 'none');
                        document.getElementById('bloqueador').style.display = 'none';
                    });
                    
                } else {
                // Realiza una solicitud AJAX para actualizar la fecha de inicio del curso.
                $.ajax({
                    url: '../PHP/Tablas/actualizar_estado_curso.php',
                    method: 'POST',
                    data: {
                        cursoId: curso.id_curso, // Envía el ID del curso
                        nuevoValor: nuevoValor, // Envía el nuevo valor (1 o 0)
                        fun: 1 //valor de control
                    },
                    success: function(response) {
                         // Actualización exitosa
                    },
                    error: function(xhr, status, error) {
                        // Maneja errores aquí
                        alert('Error al actualizar el valor en la base de datos: ' + error);
                    }
                    });
                }

            });

            crearBtnEdit(curso, fila);

            tablaBody.append(fila);
        });
    } else {
        var fila = $('<tr>');
        fila.append($('<td colspan="10">').text('No se encontraron cursos'));

        tablaBody.append(fila);
    }
}

function crearBtnEdit(curso, fila){
    // Crear un botón
    var boton = document.createElement("button");
    boton.innerHTML = "Editar";
    
    //Le agrego al boton una id correspondiente a su id en la bd
    boton.setAttribute("id", curso.id_curso);
    //Le agrego al boton la clase back-button, para que tenga un diseño
    boton.setAttribute("class", "edit-button");
    
    //Le agrego un EventListener, detecta cuando se hace click en un boton y lo envia al formulario de edicion de materia.
    boton.addEventListener("click", function() {
      window.location.href = "../HTML/Formularios/Formulario_modif_curso.html?id=" + this.id;
    });
  
    //Agregar el boton a la fila
    fila.append($('<td>').append(boton));
}

// Función para filtrar los cursos según el criterio de búsqueda
function filtrarCursos(cursos, filtro) {
    return cursos.filter(function(curso) {
        var nombreMateria = curso.materia.toLowerCase();
        var nombreProfesor = curso.profesor.toLowerCase();
        return nombreMateria.includes(filtro.toLowerCase()) || nombreProfesor.includes(filtro.toLowerCase());
    });
}

function goBack() {
    window.history.back();
}

