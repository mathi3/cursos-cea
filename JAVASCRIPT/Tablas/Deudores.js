$(document).ready(function () {
  $.ajax({
    url: '../../PHP/Tablas/Mostrar_deudores.php',
    dataType: 'json',
    success: function (data) {
      var tablaBody = $('#tabla-pagos-body');

      if (data.length > 0) {
        // Almacenar los datos de los pagos en una variable
        var pagos = data;

        // Mostrar todos los pagos al cargar la página
        mostrarAlumnos(pagos);

        // Escuchar el evento de entrada en el campo de búsqueda
        $('#input-busqueda').on('input', function () {
          var filtro = $(this).val();
          var alumnosFiltrados = filtrarAlumnos(pagos, filtro);
          mostrarAlumnos(alumnosFiltrados);
        });
      } else {
        var fila = $('<tr>');
        fila.append($('<td colspan="9">').text('No se encontraron pagos'));

        tablaBody.append(fila);
      }
    },
    error: function () {
      console.log('Error al obtener los datos de los pagos');
    }
  });

  // Agregar un evento clic al botón "Expandir"
  $('#tabla-pagos-body').on('click', '.edit-button', function () {
    // Obtener los datos del alumno correspondiente
    var idAlumno = $(this).attr('id');
    var nombre = $(this).closest('tr').find('td:eq(0)').text();
    var apellido = $(this).closest('tr').find('td:eq(1)').text();
    var fechaInscripcion;
    var deuda = $(this).closest('tr').find('td:eq(6)').text();

    $.ajax({
      url: "../../PHP/Tablas/deudores_Exp.php",
      type: "POST",
      data: {
        idAlumno: idAlumno,
        fun: 0
      }, // Enviar el ID del alumno como un objeto
      dataType: 'json', // Esperar una respuesta JSON
      success: function (data) {
        var tablaBody = $('#tabla-pagos-Alumnos-body');
        tablaBody.empty();
        if (data.length > 0) {
          // Almacenar los datos de los pagos en una variable
          var info = data;
          $.each(info, function (index, alumno) {
            var fila = $('<tr>');
            fila.append($('<td>').text(alumno.fecha_cobro).css('background-color', 'white'));
            fila.append($('<td>').text(alumno.curso).css('background-color', 'white'));
            fila.append($('<td>').text(alumno.n_cuota).css('background-color', 'white'));
            fila.append($('<td>').text("$"+alumno.monto).css('background-color', 'white'));

            tablaBody.append(fila);
          });
        } else {
          var fila = $('<tr>');
          fila.append($('<td colspan="4">').text('No se encontraron pagos').css('background-color', 'white'));

          tablaBody.append(fila);
        }
      },
      error: function () {
        console.log('Error al obtener los datos del alumno');
      }
    });

    $.ajax({
      url: "../../PHP/Tablas/deudores_Exp.php",
      type: "POST",
      data: {
        idAlumno: idAlumno,
        fun: 1
      }, // Enviar el ID del alumno como un objeto
      dataType: 'json', // Esperar una respuesta JSON
      success: function (data) {
        if (data.fecha_inscripcion !== undefined) {
            $('#fecha_inscripcion').text(data.fecha_inscripcion);
        } else {
            fechaInscripcion = 'Dato no encontrado';
            $('#fecha_inscripcion').text(fechaInscripcion);
        }
    },
      error: function () {
        console.log('Error al obtener los datos del alumno (fun1)');
        console.log(idAlumno);
      }
    });

    $.ajax({
      url: "../../PHP/Tablas/deudores_Exp.php",
      type: "POST",
      data: {
        idAlumno: idAlumno,
        fun: 2
      }, // Enviar el ID del alumno como un objeto
      dataType: 'json', // Esperar una respuesta JSON
      success: function (data) {
        var tablaBody = $('#tabla-deudas-Alumnos-body');
        tablaBody.empty();
        if (data.length > 0) {
          // Almacenar los datos de los pagos en una variable
          var info = data;
          $.each(info, function (index, curso) {
            var fila = $('<tr>');
            fila.append($('<td>').text(curso.descripcion_curso).css('background-color', 'white'));
            fila.append($('<td>').text(curso.cuotas_pendientes).css('background-color', 'white'));
            fila.append($('<td>').text("$"+curso.valor).css('background-color', 'white'));
            fila.append($('<td>').text("$"+curso.total_deuda).css('background-color', 'white'));

            tablaBody.append(fila);
          });
        } else {
          var fila = $('<tr>');
          fila.append($('<td colspan="3">').text('No se encontraron deudas').css('background-color', 'white'));

          tablaBody.append(fila);
        }
      },
      error: function () {
        console.log('Error al obtener los datos del alumno');
      }
    });

    // Mostrar el formulario y llenar los campos con los datos del alumno
    $('#nombre').text(nombre);
    $('#apellido').text(apellido);
    $('#fecha_inscripcion').text(fechaInscripcion);
    $('#deuda').text(deuda);
    $('.form-container').css('display', 'flex');

    // Bloquear el fondo detrás del formulario
    $('body').css('overflow', 'hidden');
    document.getElementById('bloqueador').style.display = 'block';
  });

  // Función para mostrar los pagos en la tabla
  function mostrarAlumnos(alumnos) {
    var tablaBody = $('#tabla-pagos-body');
    tablaBody.empty();

    if (alumnos.length > 0) {
      $.each(alumnos, function (index, alumno) {
        if (alumno.ultimo_cobro === null) {
          alumno.ultimo_cobro = "No pago";
        }
        var fila = $('<tr>');
        fila.append($('<td>').text(alumno.nombre));
        fila.append($('<td>').text(alumno.apellido));
        fila.append($('<td>').text(alumno.ultimo_cobro));
        fila.append($('<td>').text(alumno.cant_cursos));
        fila.append($('<td>').text(alumno.cant_cuotas));
        fila.append($('<td>').text(alumno.cuotas_pagas));
        fila.append($('<td>').text("$"+alumno.deuda_total));

        crearBtnExp(alumno, fila);

        tablaBody.append(fila);
      });
    } else {
      var fila = $('<tr>');
      fila.append($('<td colspan="9">').text('No se encontraron pagos'));

      tablaBody.append(fila);
    }
  }

  // Función para filtrar los pagos según el criterio de búsqueda
  function filtrarAlumnos(pagos, filtro) {
    return pagos.filter(function (pago) {
      var nombreCompleto = pago.nombre.toLowerCase() + ' ' + pago.apellido.toLowerCase();
      return nombreCompleto.startsWith(filtro.toLowerCase());
    });
  }

  function crearBtnExp(alumno, fila) {
    // Crear un botón
    var boton = document.createElement("button");
    boton.innerHTML = "Expandir";

    //Le agrego al boton una id correspondiente a su id en la bd
    boton.setAttribute("id", alumno.id_alumno);
    //Le agrego al boton la clase back-button, para que tenga un diseño
    boton.setAttribute("class", "edit-button");

    //Agregar el boton a la fila
    fila.append($('<td>').append(boton));
  }
});