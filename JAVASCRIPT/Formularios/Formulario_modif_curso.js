//CARGA DE DATOS EN EL FORM
var urlParams = new URLSearchParams(window.location.search);
var id = urlParams.get('id');

console.log(id);

$(document).ready(function() {

  var formData= new FormData;
  formData.append('id', id);
  var materiaPrecargada= "";
  var profesorPrecargado= "";

  $.ajax({
    url: '../../PHP/Formulario_modif_curso.php',
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function(data) {
      //Guardo la referencia del formulario
      var formulario = document.getElementById("Formulario_cursos");
      console.log(data);
      //Cargo los datos de la BD en el formulario
      formulario.querySelector('#nombre').value=data.descripcion_curso;

      var option = document.createElement('option');
      option.value = data.id_materia;
      option.textContent = data.materia;
      formulario.querySelector('#materia').appendChild(option);  
      materiaPrecargada=data.id_materia;

      var option2 = document.createElement('option');
      option2.value=data.id_profesor;
      option2.textContent = data.nombre;
      formulario.querySelector('#profesor').appendChild(option2);
      profesorPrecargado=data.id_profesor;

      formulario.querySelector('#fecha_inicio').value=data.fecha_inicio;
  
      formulario.querySelector('#horario').value=data.horario;

      formulario.querySelector('#dia').value=data.dia;
    },
    error: function() {
      console.log('Error al obtener los datos del curso');
    }
  });

      // Llenar el desplegable de Materias
      fetch('../../PHP/Formularios/funcion_obtener_materia.php')
      .then(response => response.json())
      .then(data => {
        var desplegableMateria = document.getElementById('materia');
        data.forEach(jsonMateria => {
          if(jsonMateria.id_materia!=materiaPrecargada){
          var optionM = document.createElement('option');
          optionM.value = jsonMateria.id_materia;
          optionM.text = jsonMateria.nombre_materia;
          desplegableMateria.appendChild(optionM);
        }
        });
      })
      .catch(error => console.error('Error:', error));

      // Llenar el desplegable de Profesores
      fetch('../../PHP/Formularios/funcion_obtener_profesor.php')
      .then(response => response.json())
      .then(data => {
        var desplegableProfesor = document.getElementById('profesor');
        data.forEach(jsonProfesor => {
          if(jsonProfesor.id_profesor!=profesorPrecargado){
          var option = document.createElement('option');
          option.value = jsonProfesor.id_profesor;
          option.text = jsonProfesor.nombre;
          desplegableProfesor.appendChild(option);
          }
        });
      })
      .catch(error => console.error('Error:', error));

  // Capturar el evento de envío del formulario
  $("#Formulario_cursos").submit(function(event) {
      event.preventDefault(); // Evitar el envío del formulario
      
      // Obtener los datos del formulario y convertirlos en un objeto JavaScript
      var formData = {
        nombre: $("#nombre").val(), // Reemplaza "nombre" con el nombre del campo en tu formulario
        materia: $("#materia").val(), // Reemplaza "materia" con el nombre del campo en tu formulario
        profesor: $("#profesor").val(), // Reemplaza "profesor" con el nombre del campo en tu formulario
        fecha_inicio: $("#fecha_inicio").val(), // Reemplaza "fecha_inicio" con el nombre del campo en tu formulario
        dia: $("#dia").val(), // Reemplaza "dia" con el nombre del campo en tu formulario
        horario: $("#horario").val(), // Reemplaza "horario" con el nombre del campo en tu formulario
        id: id
      };

      // Convertir el objeto en una cadena JSON
      var jsonData = JSON.stringify(formData);

      console.log(jsonData);
      // Enviar los datos al servidor mediante AJAX
      $.ajax({
          url: "../../PHP/actualizar_curso.php",
          type: "POST",
          data: jsonData, // Usar la cadena JSON
          contentType: "application/json; charset=utf-8", // Especificar el tipo de contenido
          dataType: "json", // Esperar una respuesta JSON del servidor
          success: function() {
          //Se muestra un mensaje de exito y se termina la actualizacion
          alert("Modificación de curso guardada exitosamente.");
          $("#Formulario_cursos")[0].reset();
                 
          // Redirigir a otra página
          window.location.href = "../Cursos.html";
          },
    
            
          error: function(xhr, status, error) {
              if (xhr.status == 400) {
                alert("Por favor, rellene todos los campos.");
              } else if (xhr.status == 500) {
                alert("Error en el servidor.");
              } else {
                alert("Ha ocurrido un error inesperado");
              }
            }
          });
        });
});

//Volver atras
function goBack() {
  window.history.back();
}