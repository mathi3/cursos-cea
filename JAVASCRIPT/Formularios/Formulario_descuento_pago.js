import { cargarDescuentos } from "../Funciones/funciones.js"

$(document).ready(function () {
    
    cargarDescuentos(function (descuentos) {
        //Cargo los valores de los descuentos en los input del formulario
        document.getElementById("efectivo").value = ((descuentos[0].porcentaje)*100).toFixed(0);
        document.getElementById("banco").value = ((descuentos[1].porcentaje)*100).toFixed(0);
        document.getElementById("mercadoPago").value = ((descuentos[2].porcentaje)*100).toFixed(0);
    });

  // Capturar el evento de envío del formulario
  $("#miFormulario").submit(function (event) {
    event.preventDefault(); // Evitar el envío del formulario

    // Obtener los datos del formulario
    var formData = $(this).serialize();
    console.log(formData);

    // Enviar los datos al servidor mediante AJAX
    $.ajax({
      url: "../../PHP/actualizar_descuentos.php",
      type: "POST",
      data: formData,
      success: function (response) {
        alert("Los datos se cargaron correctamente.");
        
        // Redirigir a otra página
        window.location.href = "../Administrar.html";

        $("#registration-form")[0].reset();
      },
      error: function (xhr, status, error) {
        if (xhr.status == 400) {
          alert("Por favor, rellene todos los campos.");
        } else if (xhr.status == 500) {
          alert("Error en el servidor.");
        } else {
          alert("Ha ocurrido un error inesperado");
        }
      }
    });
  });
});