$(document).ready(function() {
  // Capturar el evento de envío del formulario
  $("#formulario_usuario").submit(function(event) {
    event.preventDefault(); // Evitar el envío del formulario

    // Obtener los datos del formulario
    var formData = $(this).serialize();

    // Enviar los datos al servidor mediante AJAX
    $.ajax({
      url: "../../PHP/Formulario_alta_usuarios.php", // Agregar extensión ".php" al final del archivo
      type: "POST",
      data: formData,
      success: function(response) {
        // Procesar la respuesta del servidor

        // Redirigir a otra página
        window.location.href = "../../HTML/Pagina_principal.html";
        console.log(response);
        // Realizar otras operaciones necesarias

        // Por ejemplo, mostrar un mensaje de éxito
        alert("Registro de Usuario guardado exitosamente.");

        $("#formulario_usuario")[0].reset();
      },
      error: function(xhr, status, error) {
        if (xhr.status == 400) {
          alert("Por favor, rellene todos los campos.");
        } else if (xhr.status == 500) {
          alert("Error en el servidor.");
        } else {
          alert("Ha ocurrido un error inesperado");
        }
      }
    });
  });
});
function goBack() {
  window.history.back();
}
