import { obtenerFechaActual } from "../Funciones/funciones.js"

$(document).ready(function () {
    //Cargo fecha actual en el campo "fecha"
    document.getElementById('fecha').value = obtenerFechaActual();
});

// Obtén el formulario y agrega un evento 'submit' a él
var formulario = document.getElementById("miFormulario");

formulario.addEventListener("submit", function(event) {
    event.preventDefault(); // Evita el envío del formulario

    // Obtiene los valores de los campos del formulario
    var nombre = document.getElementById("nombre").value;
    var categoria = document.getElementById("categoria").value;
    var fecha = document.getElementById("fecha").value;
    var monto = document.getElementById("monto").value;

    // Realizar la solicitud AJAX
    $.ajax({
        url: "../../PHP/Formulario_gastos.php",
        method: "POST",
        data: {
            nombre: nombre,
            categoria: categoria,
            fecha: fecha,
            monto: monto,
        },
        success: function(response) {
            alert("Pago registrado correctamente.");
            // Redirigir a otra página
            window.location.href = "../../HTML/Pagos.html";
            formulario.reset(); // Limpiar el formulario

        },
        error: function(xhr, status, error) {
            alert("Error al registrar el pago: " + error);
        }
    });

});