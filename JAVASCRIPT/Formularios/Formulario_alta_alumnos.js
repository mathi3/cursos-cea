import { obtenerFechaActual } from "../Funciones/funciones.js"

$(document).ready(function () {
  //Cargo fecha actual en el campo "fecha"
  document.getElementById('fecha-inscripcion').value = obtenerFechaActual();

  // Capturar el evento de envío del formulario
  $("#registration-form").submit(function (event) {
    event.preventDefault(); // Evitar el envío del formulario

    // Obtener los datos del formulario
    var formData = $(this).serialize();
    console.log(formData);

    // Enviar los datos al servidor mediante AJAX
    $.ajax({
      url: "../../PHP/Php_alta_alumnos.php",
      type: "POST",
      data: formData,
      success: function () {
        // Procesar la respuesta del servidor

        // Realizar otras operaciones necesarias

        // Por ejemplo, mostrar un mensaje de éxito
        alert("Registro de alumno guardado exitosamente.");

        $("#registration-form")[0].reset();

        // Redirigir a otra página
        window.location.href = "../Cursos.html";
      },
      error: function (xhr, status, error) {
        if (xhr.status == 400) {
          alert("Por favor, rellene todos los campos.");
        } else if (xhr.status == 500) {
          alert("Error en el servidor.");
        } else {
          alert("Ha ocurrido un error inesperado");
        }
      }
    });
    window.location.href = "../../HTML/Formularios/Formulario_asigno_curso.html";
  });
});