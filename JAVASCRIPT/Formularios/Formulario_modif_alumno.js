//CARGA DE DATOS EN EL FORM
var urlParams = new URLSearchParams(window.location.search);
var id = urlParams.get('id');

$(document).ready(function() {

  var formData= new FormData;
  formData.append('id', id);

  $.ajax({
    url: '../../PHP/Formulario_modif_alumnos.php',
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function(data) {
      //Guardo la referencia del formulario
      var formulario = document.getElementById("Formulario_alumno");

      //Cargo los datos de la BD en el formulario
      formulario.querySelector('#nombre').value=data.nombre;

      formulario.querySelector('#apellido').value=data.apellido;
  
      formulario.querySelector('#dni').value=data.dni;

      formulario.querySelector('#fecha_inscripcion').value=data.fecha_inscripcion;
  
      formulario.querySelector('#fecha_nacimiento').value=data.fecha_nacimiento;

      formulario.querySelector('#direccion').value=data.direccion;

      formulario.querySelector('#codigo_postal').value=data.codigo_postal;

      formulario.querySelector('#telefono').value=data.telefono;

      formulario.querySelector('#email').value=data.email;

      formulario.querySelector('#empresa').value=data.empresa;
      
      formulario.querySelector('#datos_padres').value=data.datos_padres;

      formulario.querySelector('#observacion').value=data.observacion;
  },
    error: function() {
      console.log('Error al obtener los datos de las materias');
    }
  });

  // Capturar el evento de envío del formulario
  $("#Formulario_alumno").submit(function(event) {
      event.preventDefault(); // Evitar el envío del formulario
      
      // Obtener los datos del formulario
      var formData = $(this).serialize();
      formData += '&id=' + id; // Agregar el campo 'id' a los datos
      console.log(formData);
      // Enviar los datos al servidor mediante AJAX
      $.ajax({
          url: "../../PHP/actualizar_alumno.php",
          type: "POST",
          data: formData,
          success: function() {                      
          // Redirigir a otra página
          window.location.href = "../Tablas/Mostrar_alumnos.html";

          //Se muestra un mensaje de exito y se termina la actualizacion
          alert("Modificación de alumno guardada exitosamente.");
          $("#Formulario_alumno")[0].reset();
          },
    
          error: function(xhr, status, error) {
              if (xhr.status == 400) {
                alert("Por favor, rellene todos los campos.");
              } else if (xhr.status == 500) {
                alert("Error en el servidor.");
              } else {
                alert("Ha ocurrido un error inesperado");
              }
            }
          });
        });
});

//Volver atras
function goBack() {
  window.history.back();
}