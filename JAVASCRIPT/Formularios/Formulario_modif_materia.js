//CARGA DE DATOS EN EL FORM
var urlParams = new URLSearchParams(window.location.search);
var id = urlParams.get('id');

$(document).ready(function() {

  var formData= new FormData;
  formData.append('id', id);

  $.ajax({
    url: '../../PHP/Formulario_modif_materias.php',
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function(data) {
      //Guardo la referencia del formulario
      var formulario = document.getElementById("Formulario_materias");

      //Cargo los datos de la BD en el formulario
      formulario.querySelector('#nombre_materia').value=data.nombre_materia;

      formulario.querySelector('#cuotas').value=data.cuotas;
  
      formulario.querySelector('#valor').value=data.valor;

      formulario.querySelector('#valor_bonif').value=data.valor2;
  
      formulario.querySelector('#descripcion').value=data.descripcion;
  },
    error: function() {
      console.log('Error al obtener los datos de las materias');
    }
  });

  // Capturar el evento de envío del formulario
  $("#Formulario_materias").submit(function(event) {
      event.preventDefault(); // Evitar el envío del formulario
      
      // Obtener los datos del formulario
      var formData = $(this).serialize();
      formData += '&id=' + id; // Agregar el campo 'id' a los datos
      console.log(formData);
      // Enviar los datos al servidor mediante AJAX
      $.ajax({
          url: "../../PHP/actualizar_materia.php",
          type: "POST",
          data: formData,
          success: function() {
          //Se muestra un mensaje de exito y se termina la actualizacion
          alert("Modificación de materia guardada exitosamente.");
          $("#Formulario_materias")[0].reset();
                        
          // Redirigir a otra página
          window.location.href = "../Tablas/Mostrar_materias.html";
          },
    
            
          error: function(xhr, status, error) {
              if (xhr.status == 400) {
                alert("Por favor, rellene todos los campos.");
              } else if (xhr.status == 500) {
                alert("Error en el servidor.");
              } else {
                alert("Ha ocurrido un error inesperado");
              }
            }
          });
        });
});

//Volver atras
function goBack() {
  window.history.back();
}