$(document).ready(function() {
    // Capturar el evento de envío del formulario
    $("#Formulario_materias").submit(function(event) {
      event.preventDefault(); // Evitar el envío del formulario
  
      // Obtener los datos del formulario
      var formData = $(this).serialize();
   
      // Enviar los datos al servidor mediante AJAX
      $.ajax({
        url: "../../PHP/Formularios/Formulario_alta_materia.php",
        type: "POST",
        data: formData,
        success: function(response) {
          // Procesar la respuesta del servidor

          // Realizar otras operaciones necesarias
  
          // Por ejemplo, mostrar un mensaje de éxito
          alert("Registro de materia guardado exitosamente.");
          $("#Formulario_materias")[0].reset();
                    
          // Redirigir a otra página
                    window.location.href = "../../HTML/Tablas/Mostrar_materias.html";
                    console.log(response);
        },

        
        error: function(xhr, status, error) {
          if (xhr.status == 400) {
            alert("Por favor, rellene todos los campos.");
          } else if (xhr.status == 500) {
            alert("Error en el servidor.");
          } else {
            alert("Ha ocurrido un error inesperado");
          }
        }
      });
    });
  });
  function goBack() {
    window.history.back();
}
