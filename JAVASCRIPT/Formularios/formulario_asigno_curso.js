$(document).ready(function() {
  var alumnoSelect = $("#alumno");
  var cursoSelect = $("#curso");
  var becaInput = document.getElementById("beca")
  var form = $("#asignacion");
  
    // Initialize Select2 for the "alumno" select element
    $("#alumno").select2();

    // Initialize Select2 for the "curso" select element
    $("#curso").select2();

  // Cargar alumnos desde el servidor
  $.ajax({
    url: "../../PHP/Formularios/funcion_obtener_alumno.php",
    method: "GET",
    dataType: "json",
    success: function (data) {
        
        // Llenar el selector de alumnos con los datos recibidos
        $.each(data, function (index, alumno) {
            alumnoSelect.append("<option value='" + alumno.id_alumno + "'>" + alumno.nombre + " " + alumno.apellido + "</option>");
        });
    },
    error: function (xhr, status, error) {
        console.log("Error al obtener los alumnos:", error);
    }
});
 
  // Cargar cursos desde el servidor
  $.ajax({
    url: "../../PHP/Funcion_obtener_curso.php",
    method: "GET",
    dataType: "json",
    success: function(data) {
      $.each(data, function(index, curso) {
        cursoSelect.append("<option value='" + curso.id_curso + "'>" + curso.descripcion_curso + "</option>");
      });
    },
    error: function(xhr, status, error) {
      console.log("Error al obtener los cursos:", error);
    }
  });

  // Capturar el evento de envío del formulario
  $("#asignacion").submit(function(event) {
    event.preventDefault(); // Evitar el envío del formulario

    // Obtener el ID del curso seleccionado en el formulario
    var selectedCursoId = cursoSelect.val();

    // Crear FormData y agregar los valores calculados
    var formData = new FormData(form[0]); // Obtener los datos del formulario
    formData.append("beca", becaInput.value); //Agrego el valor de la beca

    console.log(becaInput.value);
    // Enviar los datos al servidor mediante AJAX
    $.ajax({
      url: "../../PHP/Formularios/Formulario_asigno_curso.php",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function(response) {
        //var obj = JSON.parse(response);
        console.log(response); // Imprime el valor del primer parámetro
        if(response.success === false){
          alert("El estudiante ya esta inscripto en el curso seleccionado.");
        } else {
          alert("Datos cargados correctamente");
                        
          // Redirigir a otra página
          window.location.href = "../Cursos.html";
        }

        $("#asignacion")[0].reset();
      },
      error: function(xhr, status, error) {
          console.log("Error en la solicitud AJAX: " + error);
  
      }
      });
      
    });
  
    // Initialize Select2 for the "alumno" select element
    $("#alumno").select2();

    // Initialize Select2 for the "curso" select element
    $("#curso").select2();
});
function goBack() {
  window.history.back();
}