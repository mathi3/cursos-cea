import { obtenerFechaActual, cargarDescuentos } from "../Funciones/funciones.js";

function volver() {
    window.location.href = "../../HTML/Pagos.html";
}

// Objeto para almacenar el último número de cuota por campoCurso
var ultimoNumeroCuota = {};
// Objeto para almacenar los cursos por alumno
var cursosSeleccionados = {}; // --> POR HACER


$(document).ready(function () {
    // Initialize Select2 for the "alumno" select element
    $("#alumno").select2();

    var numeroCampos = 1;
    var numeroCamposMax = 5;
    var agregarCursoButton = document.getElementById("agregarCurso");

    verifBorrarCurso(numeroCampos);

    agregarCursoButton.addEventListener("click", function () {
        if (numeroCampos < numeroCamposMax) {
            // Activar el botón borrarCurso
            document.getElementById("borrarCurso").disabled = false;
            numeroCampos++;
            var contenedorCursos = document.getElementById("contenedorCursos");
            var nuevoCampoCurso = document.createElement("div");
            nuevoCampoCurso.id = "campoCurso" + numeroCampos;

            nuevoCampoCurso.innerHTML = `
            <br>
            <label for="curso${numeroCampos}">Curso ${numeroCampos}:</label>
            <select name="curso${numeroCampos}" id="curso${numeroCampos}" style="width: 100%;" required>
                <option value="" disabled selected>Seleccione un curso</option>
            </select>
            <table id="tabla-cuotas${numeroCampos}" style="display: none; width: 100%; max-height: 10rem; overflow-y: auto;">
                <thead>
                  <tr>
                    <th style="width: 17%;">N° Cuota</th>
                    <th style="width: 30%;">Descripción</th>
                    <th style="width: 30%"> Limite Bonif.</th>
                    <th style="width: 15%;">Bonificado</th>
                    <th style="width: 13%;">Monto</th>
                    <th style="width: 5%;"></th>
                  </tr>
                </thead>
                <tbody id="tabla-cursos${numeroCampos}-body">
                </tbody>
              </table>
              <label for="subtotal${numeroCampos}" style="margin-top: 1rem;">Subtotal ${numeroCampos}: <span id="subtotal${numeroCampos}"></span></label>
            `;

            ultimoNumeroCuota[numeroCampos] = null;
            contenedorCursos.appendChild(nuevoCampoCurso);
            nuevoCampoCurso.style.display = "block";
            $(`#curso${numeroCampos}`).select2();

            if (numeroCampos === numeroCamposMax) {
                // Desactivar el botón
                document.getElementById("agregarCurso").disabled = true;
            }
            actualizarCursos(numeroCampos);
        } else {
            alert("Ya no se pueden agregar más cursos.");
        }
    });

    var borrarCursoButton = document.getElementById("borrarCurso");

    borrarCursoButton.addEventListener("click", function () {
        if (numeroCampos > 1) {

            //Función para borrar el div CampoCurso correspondiente
            eliminarCampoCurso(numeroCampos);
            
            //Resto 1 a numeroCampos.
            numeroCampos--;
            
            verifBorrarCurso(numeroCampos);
            
            if (numeroCampos <= (numeroCamposMax-1)) {
                // Desactivar el botón
                document.getElementById("agregarCurso").disabled = false;
            }
        } else {
            alert("Ya no se pueden eliminar más cursos.");
        }
    });

    document.getElementById("fechaLabel").textContent = `${obtenerFechaActual()}`;

    var alumnoSelect = $("#alumno");
    
    $("#descu").attr("data-valor", 1);
    calcuDescu(0);

    $.ajax({
        url: "../../PHP/Formularios/funcion_obtener_alumno.php",
        method: "GET",
        dataType: "json",
        success: function (data) {
            $.each(data, function (index, alumno) {
                alumnoSelect.append("<option value='" + alumno.id_alumno + "'>" + alumno.nombre + " " + alumno.apellido + "</option>");
            });
        },
        error: function (xhr, status, error) {
            console.log("Error al obtener los alumnos:", error);
        }
    });

    $("#alumno").change(function () {
        document.getElementById("monto").textContent = "";
        var selectedAlumnoId = $(this).val(); // Obtener el ID del alumno seleccionado
        $("#tipoPago").prop("disabled", true);
        $('[id^="pagParcial"]').prop("disabled", true);
        //Desmarco todos los checkbox para evitar error de calculo
        $(".checkboxCurso").prop("checked", false);

        // Realizar una solicitud AJAX para obtener el curso vinculado a ese alumno
        $('[id^="curso"]').each(function () {
            var campoCursoId = this.id;

            //Oculto las tablas de los cursos
            var idC = campoCursoId.charAt(campoCursoId.length - 1);
            $(`#tabla-cuotas${idC}`).css("display", "none");
            //Reseteo el subtotal a 0
            $("#subtotal"+idC).text(0);

            $.ajax({
                url: "../../PHP/Funcion_obtener_curso.php",
                method: "GET",
                dataType: "json",
                data: {
                    alumno_id: selectedAlumnoId
                },
                success: function (data) {
                    // Llenar el selector de cursos con la descripción del curso obtenido
                    $("#" + campoCursoId).empty();
                    $("#" + campoCursoId).append("<option value='' disabled selected>Seleccione un curso</option>");
                    
                    //Verificación para evitar mas CampoCurso que los cursos en los que esta inscripto un alumno
                    numeroCamposMax=data.length;
                    if(numeroCampos>numeroCamposMax){
                        for(var i = numeroCampos; i > numeroCamposMax; i--){
                            eliminarCampoCurso(i);
                            numeroCampos=i-1;
                        }

                        verifBorrarCurso(numeroCampos);
                    }

                    //Verificacion para los botones
                    if(numeroCampos<numeroCamposMax){
                        document.getElementById("agregarCurso").disabled = false;
                    } else {
                        // Desactivar el botón
                        document.getElementById("agregarCurso").disabled = true;
                    }

                    if (data.length > 0) {
                        $.each(data, function (index, curso) {
                            $("#" + campoCursoId).append("<option value='" + curso.id_curso + "'>" + curso.descripcion_curso + "</option>");
                        });
                    } else {
                        $("#" + campoCursoId).append("<option value='' disabled>No hay cursos vinculados</option>");
                    }
                },
                error: function (xhr, status, error) {
                    $("#" + campoCursoId).empty();
                    $("#" + campoCursoId).append("<option value='' disabled selected>Seleccione un curso</option>");
                    $("#" + campoCursoId).append("<option value='' disabled>No hay cursos vinculados</option>");
                    console.log("Error al obtener el curso:", error);

                    for(var i= numeroCampos; i >= 2; i--){
                        eliminarCampoCurso(i);
                        numeroCampos=i-1;
                    }

                    document.getElementById("agregarCurso").disabled = true;
                    document.getElementById("borrarCurso").disabled = true;
                }
            });
        });
    });

    // Registrar el evento 'change' en el documento para todos los elementos cuyo ID comienza con "curso"
    $(document).on('change', '[id^="curso"]', function () {
        var selectedAlumnoId = $("#alumno").val();
        var selectedCursoId = $(this).val();
        var campoCursoId = this.id;
        var idC = campoCursoId.charAt(campoCursoId.length - 1);
        var cursoSeleccionado = $(this).val();
        var cursoRepetido = false;

        // Verificar si el curso ya se seleccionó en otro campo
        for (var campo in cursosSeleccionados) {
            if (cursosSeleccionados[campo] === cursoSeleccionado && campo !== idC) {
                alert("Este curso ya ha sido seleccionado en otro campo.");
                $(this).val(null).trigger("change"); // Reiniciar la selección
                cursoRepetido = true;
                break;
            }
        }

        if (!cursoRepetido) {
            cursosSeleccionados[idC] = cursoSeleccionado; // Almacena el curso seleccionado en este campo
            $("#subtotal"+idC).text(0);

            // Mostrar la tabla nuevamente (si es necesario)
            $(`#tabla-cuotas${idC}`).css("display", "block");

            $.ajax({
                url: "../../PHP/obtener_deuda.php",
                method: "GET",
                dataType: "json",
                data: {
                    alumno_id: selectedAlumnoId,
                    curso_id: selectedCursoId
                },
                success: function (data) {
                    $("#tipoPago").prop("disabled", true);
                    if (data.length > 0) {                
                            $("#tipoPago").prop("disabled", false);
                            $.each(data, function (index, deuda) {
                                $(`#tabla-cuotas${idC}`).css("display", "block");
                                var tablaBody = $(`#tabla-cursos${idC}-body`);
                                tablaBody.empty();
                                var pr = true;
                                var bonificado = "No";

                                // Obtener la fecha actual
                                var fechaActual = new Date();
                                
                                // Pasar la fecha del PHP al formato fecha de JavaScript
                                var fechaDeuda = new Date(deuda.fecha_inicio);
                                
                                // Calcular si la fecha actual está dentro de los primeros 10 días del mes
                                var primeros10Días = fechaActual.getDate() >= 1 && fechaActual.getDate() <= 10;
                                
                                // Agregar columna para cuotas impagas para calcular hasta cuál tiene el valor1 y hasta cuál el valor2
                                for (var j = Number(deuda.cuotas_pagas) + 1; j <= Number(deuda.cant_cuotas); j++) {
                                    var fila = $('<tr>');
                                    fila.append($('<td>').text(j));
                                    fila.append($('<td>').text("Cuota N° " + j));
                                    
                                    // Calcular el mes y año correspondientes a la cuota
                                    var mesCuota = fechaDeuda.getMonth(); // Mes de inicio del curso;
                                    var añoCuota = fechaDeuda.getFullYear(); // Año de inicio del curso;
                                    
                                    for (var i = 1; i < j; i++) {
                                        mesCuota++;
                                        if (mesCuota > 11) {
                                            mesCuota = 0; // Volver a enero si llegamos a diciembre
                                            añoCuota++; // Incrementar el año si llegamos a enero
                                        }
                                    }
                                    
                                    // Armar la fecha en formato de texto (puedes ajustar el formato según tus necesidades)
                                    var fechaLimite = "10/" + (mesCuota + 1) + "/" + añoCuota; // Sumar 1 al mes para ajustar a formato de calendario (enero es 1, no 0)

                                    fila.append($('<td>').text(fechaLimite));

                                    var cuotaValor = deuda.valor; // Valor por defecto (después del día 10)
                                    
                                    // Verificar si estamos dentro de los primeros 10 días del mes correspondiente
                                    if ((primeros10Días && fechaActual.getMonth() === mesCuota && fechaActual.getFullYear() === añoCuota) || 
                                        (fechaActual.getFullYear() < añoCuota || (fechaActual.getFullYear() === añoCuota && fechaActual.getMonth() < mesCuota))) {
                                        cuotaValor = deuda.valor2; // Aplicar valor2
                                        bonificado="Si";
                                    }

                                    fila.append($('<td>').text(bonificado));
                                    fila.append($('<td>').text(cuotaValor));
                                    
                                    var checkbox = crearCheck(j, idC);
                                    fila.append(checkbox);
                                    tablaBody.append(fila);
                                    pr = false;

                                }
                            });
                        }
                },         
            error: function (xhr, status, error) {
                console.log("Error al obtener la deuda:", error);
            }
        });
    } else {
        // Limpiar y ocultar la tabla
        $(`#tabla-cuotas${idC}`).css("display", "none");
        $(`#tabla-cuotas${idC}`).find("tbody").empty(); // Esto limpia el contenido de la tabla
    }
    cursoRepetido=false;
    });

    // Agregar evento change a los checkboxes
    $(document).on("change", ".checkboxCurso", function () {
        // Obtener todas las clases del elemento que disparó el evento
        var clases = this.className.split(" ");
        var numeroCurso;

        for (var i = 0; i < clases.length; i++) {
            if (clases[i].startsWith("checkCurso")) {
                // Extraer el número X de la clase
                numeroCurso = clases[i].charAt(clases[i].length -1);
               
            }
        }

        // Lógica para marcar checkboxes superiores
        var checkboxId = this.id; // Obtener el ID del checkbox actual
        var checkboxIndex = $(`#${checkboxId}`).closest("tr").index(); // Obtener el índice de la fila actual

        if ($(this).prop("checked")) {
            // Marcar los checkboxes anteriores
            $(`.checkCurso${numeroCurso}`).each(function (index) {
                if (index < checkboxIndex) {
                    $(this).prop("checked", true);
                }
            });
        } else {
            // Desmarcar los checkboxes anteriores
            $(`.checkCurso${numeroCurso}`).each(function (index) {
                if (index > checkboxIndex) {
                    $(this).prop("checked", false);
                }
            });
        }

        // Llamar a la función general para calcular el subtotal
        calcularSubtotal(numeroCurso);
    }); 

    $("#tipoPago").change(function () {
        calcuDescu(1);
    });

    $("#volverButton").click(function () {
        volver();
    });

    var formulario = document.getElementById("miFormulario");

    formulario.addEventListener("submit", function (event) {
        event.preventDefault();
        // Obtener el número de campos de curso actuales
        var numeroCamposActuales = $('[id^="curso"]').length;

        // Crear un objeto para rastrear si al menos un checkbox está marcado en cada campo de curso
        var alMenosUnCheckboxMarcado = {};

        // Recorrer cada campo de curso existente y establecer el valor inicial en falso para cada campo
        for (var i = 1; i <= numeroCamposActuales; i++) {
            alMenosUnCheckboxMarcado[i] = false;
        }

        // Recorrer cada campo de curso existente y verificar si al menos un checkbox está marcado en cada campo
        for (var i = 1; i <= numeroCamposActuales; i++) {
            var cursoId = "curso" + i;

            // Comprueba si al menos un checkbox está marcado en el campoCurso actual
            if ($(`.checkCurso${i}:checked`).length > 0) {
                alMenosUnCheckboxMarcado[i] = true;
            }
        }

        // Verifica si al menos un checkbox está marcado en cada campo
        var checkBoxMarcados = true;

        for (var i = 1; i <= numeroCamposActuales; i++) {
            if (!alMenosUnCheckboxMarcado[i]) {
                checkBoxMarcados = false;
                break; // Puedes salir del bucle tan pronto como encuentres un campo sin checkbox marcados
            }
        }

        if (!checkBoxMarcados) {
            alert("Debes marcar al menos una cuota a pagar en cada curso.");
            return;
        }
        
        // Crear un arreglo para almacenar los datos de los cursos y los números de cuotas
        var cursosYCuotas = [];

        // Recorrer cada campo de curso existente y obtener los valores
        for (var i = 1; i <= numeroCamposActuales; i++) {
            var cursoId = "curso" + i;
            var subtotalId = "subtotal" + i;

            var alumno = $("#alumno").val();
            var curso = $("#" + cursoId).val();
            var subtotal = $("#" + subtotalId).text();
            
            // Obtener el número de cuota desde el objeto 'ultimoNumeroCuota' si existe
            var numeroCuota = ultimoNumeroCuota[i] || null;

            // Obtener la fecha en la que estamos realizando el pago
            var fecha = document.getElementById("fechaLabel").textContent;

            // Obtengo el id del descuento
            var id_descu = $("#descu").attr("data-valor");
            console.log(id_descu);
            // Obtengo el descuento
            var descuento = parseFloat((document.getElementById("descu").textContent).replace('%', '')) / 100;
            console.log(descuento);

            // Agregar los datos al arreglo
            cursosYCuotas.push({
                alumno: alumno,
                curso: curso,
                subtotal: parseFloat(subtotal),
                numeroCuota: numeroCuota,
                fecha: fecha,
                id_descu: id_descu,
                descuento: descuento,
            });
        }

        // Después de construir cursosYCuotas
        var cursosYCuotasJSON = JSON.stringify(cursosYCuotas);
        
        // Realizar la solicitud AJAX
        $.ajax({
            url: "../../PHP/Formulario_pagos.php",
            method: "POST",
            data: { cursosYCuotas: cursosYCuotasJSON }, // Envía el objeto JSON como parte de los datos
            dataType: "json", // Tipo de datos esperados como respuesta (puedes cambiarlo según tus necesidades)
            success: function (response) {
                alert("Pago registrado correctamente.");
                formulario.reset(); // Limpiar el formulario
                location.reload();
            },
            error: function (xhr, status, error) {
                alert("Error al registrar el pago: " + error);
            }
        });
    });

    // Initialize Select2 for the "alumno" select element
    $("#alumno").select2();

    // Initialize Select2 for the "curso" select element
    $("#curso1").select2();
});

function crearCheck(valor, idC) {
    //Función para crear checkbox y asignarles la id del curso al que pertenecen
    var checkbox = document.createElement("input");
    checkbox.classList.add("checkboxCurso", "checkCurso"+idC);
    checkbox.type = "checkbox";
    checkbox.id = "checkbox" + valor;


    // Crea una celda para el checkbox
    var cell = document.createElement("td");
    cell.appendChild(checkbox);

    return cell; // Devuelve la celda con el checkbox
}
// Función para calcular el subtotal
function calcularSubtotal(numeroCurso) {
    var subtotal = 0;
    var numeroCuota = 0;
    $(".checkCurso"+numeroCurso+":checked").each(function () {
        var fila = $(this).closest("tr"); // Obtener la fila que contiene el checkbox
        var monto = parseFloat(fila.find("td:nth-child(5)").text()); // Obtener el monto de la fila
        console.log(monto);
        subtotal += monto; // Sumar el monto al subtotal
    });

    //Obtener el número de cuota(s) que se va a pagar
    var checkboxes = $(`.checkCurso${numeroCurso}:checked`);
    for (var i = checkboxes.length - 1; i >= 0; i--) {
        var checkbox = checkboxes[i];
        // Extraer el número de cuota desde el ID del checkbox
        var checkboxId = checkbox.id;
        var cuota = parseInt(checkboxId.replace("checkbox", ""));
        if (!isNaN(cuota)) {
            numeroCuota = cuota;
            break; // Salir del bucle una vez que se obtenga el número de cuota del último checkbox marcado
        }
    }

    // Almacena el último número de cuota para este campoCurso en el objeto
    ultimoNumeroCuota[numeroCurso] = numeroCuota;

    // Actualizar el valor del subtotal en el elemento HTML
    $("#subtotal"+numeroCurso).text(subtotal.toFixed(2)); // Puedes ajustar el formato según tus necesidades
    var id_descu = $("#descu").attr("data-valor");
    var descuento = parseFloat((document.getElementById("descu").textContent).replace('%', '')) / 100;
    if(isNaN(descuento)){
        descuento=0;
    }
    calcularTotal(descuento, id_descu);
}

// Funcion para calcular y mostrar el total
function calcularTotal(descuento, id_descu){
    var total=0;

    $('[id^="curso"]').each(function () {
        var campoCursoId = this.id;

        //Oculto las tablas de los cursos
        var idC = campoCursoId.charAt(campoCursoId.length - 1);

        total += parseFloat(document.getElementById("subtotal"+idC).textContent);

    });
    $("#monto").text(`$${total}`);

    if (descuento != 0) {
        $("#total").text("$"+(total-(total * parseFloat(descuento))));
    } else {
        $("#total").text(`$${total}`);
    }

    $("#descu").attr("data-valor", id_descu);
}

//Funcion para eliminar los campoCurso del formulario
function eliminarCampoCurso(numeroCampos){
    //Guardo el elemento a eliminar en una variable
    var elementoAEliminar = document.getElementById(`campoCurso${numeroCampos}`);
            
    //Elimino el elemento del DOM
    if (elementoAEliminar) {
        // Obtén el valor del curso seleccionado en este campo y elimínalo de la lista
        var cursoSeleccionado = numeroCampos;
        if (cursoSeleccionado) {
            delete cursosSeleccionados[cursoSeleccionado];
        }

        elementoAEliminar.parentNode.removeChild(elementoAEliminar);
        // Elimina su entrada del objeto
        delete ultimoNumeroCuota[numeroCampos];
    }
    calcularSubtotal(numeroCampos-1);
}

//Funcion que desactiva el boton borrarCurso cuando numeroCampos es igual a 1
function verifBorrarCurso(numeroCampos){
    //Verificaciones para los botones agregar y borrar curso
    if (numeroCampos === 1) {
        // Desactivar el botón
        document.getElementById("borrarCurso").disabled = true;
    }
}

//Funcion para actualizar los cursos al agregar un campoCurso con un alumno ya seleccionado
function actualizarCursos(numeroCampo) {
    var campoCursoId = `curso${numeroCampo}`;
    var selectedAlumnoId = $("#alumno").val();

    $.ajax({
        url: "../../PHP/Funcion_obtener_curso.php",
        method: "GET",
        dataType: "json",
        data: {
            alumno_id: selectedAlumnoId
        },
        success: function (data) {
            // Llenar el selector de cursos con la descripción del curso obtenido
            $("#" + campoCursoId).empty();
            $("#" + campoCursoId).append("<option value='' disabled selected>Seleccione un curso</option>");

            if (data.length > 0) {
                $.each(data, function (index, curso) {
                    $("#" + campoCursoId).append("<option value='" + curso.id_curso + "'>" + curso.descripcion_curso + "</option>");
                });
            } else {
                $("#" + campoCursoId).append("<option value='' disabled>No hay cursos vinculados</option>");
            }
        },
        error: function (xhr, status, error) {
            $("#" + campoCursoId).empty();
            $("#" + campoCursoId).append("<option value='' disabled selected>Seleccione un curso</option>");
            $("#" + campoCursoId).append("<option value='' disabled>No hay cursos vinculados</option>");
            console.log("Error al obtener el curso:", error);
        }
    });
}

// Función para cargar los descuentos
    function calcuDescu(calcularT) {
        var selectedTipoPago = $("#tipoPago").val();
        cargarDescuentos(function (descuentos) {
            var descuento = 0;
            var id_descu = "";

            if (selectedTipoPago === "efectivo") {
                descuento = parseFloat(descuentos[0].porcentaje);
                id_descu = descuentos[0].id_descuento;
            } else if (selectedTipoPago === "mercadoPago") {
                descuento = parseFloat(descuentos[2].porcentaje);
                id_descu = descuentos[2].id_descuento;
            } else {
                descuento = parseFloat(descuentos[1].porcentaje);
                id_descu = descuentos[1].id_descuento;
            }
            document.getElementById("descu").textContent = ` ${(descuento * 100).toFixed(0)}%`;

            if(calcularT===1){
                calcularTotal(descuento, id_descu);
            }
        });
    }