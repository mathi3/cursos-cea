$(document).ready(function() {
  // Llenar el desplegable de Materias
  fetch('../../PHP/Formularios/funcion_obtener_materia.php')
    .then(response => response.json())
    .then(data => {
      var desplegableMateria = document.getElementById('materia');
      data.forEach(jsonMateria => {
        var option = document.createElement('option');
        option.value = jsonMateria.id_materia;
        option.text = jsonMateria.nombre_materia;
        desplegableMateria.appendChild(option);
      });
    })
    .catch(error => console.error('Error:', error));

  // Llenar el desplegable de Profesores
  fetch('../../PHP/Formularios/funcion_obtener_profesor.php')
    .then(response => response.json())
    .then(data => {
      var desplegableProfesor = document.getElementById('profesor');
      data.forEach(jsonProfesor => {
        var option = document.createElement('option');
        option.value = jsonProfesor.id_profesor;
        option.text = jsonProfesor.nombre;
        desplegableProfesor.appendChild(option);
      });
    })
    .catch(error => console.error('Error:', error));

  // Capturar el evento de envío del formulario
  $("#Formulario_cursos").submit(function(event) {
    event.preventDefault(); // Evitar el envío del formulario

    // Obtener los datos del formulario
    var formData = $(this).serialize();
  
     // Enviar los datos al servidor mediante AJAX
     $.ajax({
      url: "../../PHP/Formularios/Formulario_alta_cursos.php",
      type: "POST",
      data: formData,
      success: function(response) {
        // Procesar la respuesta del servidor

       // Redirigir a otra página
       window.location.href = "../../HTML/Cursos.html";
        console.log(response);
        // Realizar otras operaciones necesarias

        // Por ejemplo, mostrar un mensaje de éxito
        alert("Registro de curso guardado exitosamente.");

        $("#Formulario_cursos")[0].reset();
      },
      error: function(xhr, status, error) {
        if (xhr.status == 400) {
          alert("Por favor, rellene todos los campos.");
        } else if (xhr.status == 500) {
          alert("Error en el servidor.");
        } else {
          alert("Ha ocurrido un error inesperado");
        }
      }
    });
  });
});