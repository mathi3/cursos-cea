import { obtenerFechaActual } from "../Funciones/funciones.js"

$(document).ready(function () {
  //Cargo fecha actual en el campo "fecha"
  document.getElementById('fecha-contratacion').value = obtenerFechaActual();

  // Capturar el evento de envío del formulario
  $("#formulario_registro_profesores").submit(function (event) {
    event.preventDefault(); // Evitar el envío del formulario

    // Obtener los datos del formulario
    var formData = $(this).serialize();

    // Enviar los datos al servidor mediante AJAX
    $.ajax({
      url: "../../PHP/php_alta_profesores.php",
      type: "POST",
      data: formData,
      success: function () {
        // Procesar la respuesta del servidor

        // Redirigir a otra página
        window.location.href = "../Tablas/Mostrar_profesores.html";
        // Realizar otras operaciones necesarias

        // Por ejemplo, mostrar un mensaje de éxito
        alert("Registro de profesor guardado exitosamente.");

        $("#formulario_registro_profesores")[0].reset();

      },
      error: function (xhr, status, error) {
        if (xhr.status == 400) {
          alert("Por favor, rellene todos los campos.");
        } else if (xhr.status == 500) {
          alert("Error en el servidor.");
        } else {
          alert("Ha ocurrido un error inesperado");
        }
      }
    });
  });
});
function goBack() {
  window.history.back();
}